import { Test, TestingModule } from '@nestjs/testing';
import { ParroquiasController } from './parroquias.controller';
import { ParroquiasService } from './parroquias.service';

describe('ParroquiasController', () => {
    let controller: ParroquiasController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ParroquiasController],
            providers: [ParroquiasService],
        }).compile();

        controller = module.get<ParroquiasController>(ParroquiasController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
