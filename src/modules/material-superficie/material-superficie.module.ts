import { Module } from '@nestjs/common';
import { MaterialSuperficieService } from './material-superficie.service';
import { MaterialSuperficieController } from './material-superficie.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MaterialSuperficie } from './entities/material-superficie.entity';
import { Materiale } from '../materiales/entities/materiale.entity';

@Module({
    imports: [TypeOrmModule.forFeature([MaterialSuperficie, Materiale])],
    controllers: [MaterialSuperficieController],
    providers: [MaterialSuperficieService],
})
export class MaterialSuperficieModule {}
