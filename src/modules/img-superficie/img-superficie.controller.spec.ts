import { Test, TestingModule } from '@nestjs/testing';
import { ImgSuperficieController } from './img-superficie.controller';
import { ImgSuperficieService } from './img-superficie.service';

describe('ImgSuperficieController', () => {
    let controller: ImgSuperficieController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgSuperficieController],
            providers: [ImgSuperficieService],
        }).compile();

        controller = module.get<ImgSuperficieController>(ImgSuperficieController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
