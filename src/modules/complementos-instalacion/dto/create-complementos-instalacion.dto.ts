import { IsNumber, IsString, IsUUID } from 'class-validator';
import { InstalacionesDeportiva } from '../../instalaciones-deportivas/entities/instalaciones-deportiva.entity';

export class CreateComplementosInstalacionDto {
    _id?: string;

    @IsString()
    nombreComplemento: string;

    @IsNumber()
    medidaComplemento: number;

    @IsNumber()
    cantidadComplemento: number;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;
}
