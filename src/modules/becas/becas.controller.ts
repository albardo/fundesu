import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request, Req } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { BecasService } from './becas.service';
import { CreateBecaDto } from './dto/create-beca.dto';
import { UpdateBecaDto } from './dto/update-beca.dto';

@Controller('becas')
@ApiTags('Becas')
export class BecasController {
    constructor(private readonly becasService: BecasService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateBecaDto] })
    create(@Request() req, @Res() Res, @Body() createBecaDto: CreateBecaDto) {
        this.becasService
            .create(createBecaDto, req.user)
            .then((beca) => {
                return Res.status(HttpStatus.CREATED).json(beca);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.becasService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.becasService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateBecaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateBecaDto: UpdateBecaDto) {
        this.becasService
            .update(id, updateBecaDto)
            .then((beca) => {
                return Res.status(HttpStatus.ACCEPTED).json(beca);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Req() req, @Param('id') id: string) {
        return this.becasService.remove(id, req.user);
    }
}
