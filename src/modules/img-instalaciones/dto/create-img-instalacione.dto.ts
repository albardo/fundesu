import { IsString, IsUUID } from 'class-validator';
import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';

export class CreateImgInstalacioneDto {
    _id?: string;

    @IsString()
    image?: string;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;
}
