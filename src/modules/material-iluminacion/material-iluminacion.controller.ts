import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { MaterialIluminacionService } from './material-iluminacion.service';
import { CreateMaterialIluminacionDto } from './dto/create-material-iluminacion.dto';
import { UpdateMaterialIluminacionDto } from './dto/update-material-iluminacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('material-iluminacion')
@ApiTags('Material-iluminacion')
export class MaterialIluminacionController {
    constructor(private readonly materialIluminacionService: MaterialIluminacionService) {}

    @Post()
    @ApiBody({ type: [CreateMaterialIluminacionDto] })
    create(@Res() Res, @Body() createMaterialIluminacionDto: CreateMaterialIluminacionDto) {
        this.materialIluminacionService
            .create(createMaterialIluminacionDto)
            .then((iluminacion) => {
                return Res.status(HttpStatus.CREATED).json(iluminacion);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.materialIluminacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.materialIluminacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMaterialIluminacionDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateMaterialIluminacionDto: UpdateMaterialIluminacionDto) {
        this.materialIluminacionService
            .update(id, updateMaterialIluminacionDto)
            .then((iluminacion) => {
                return Res.status(HttpStatus.ACCEPTED).json(iluminacion);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.materialIluminacionService.remove(id);
    }
}
