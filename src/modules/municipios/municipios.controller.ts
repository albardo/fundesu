import { Controller, Get, Post, Body, Param, Res, HttpStatus, Patch, Delete, Query } from '@nestjs/common';
import { MunicipiosService } from './municipios.service';
import { CreateMunicipioDto } from './dto/create-municipio.dto';
import { UpdateMunicipioDto } from './dto/update-municipio.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('municipios')
@ApiTags('Municipios')
export class MunicipiosController {
    constructor(private readonly municipiosService: MunicipiosService) {}

    @Post('/')
    @ApiBody({ type: [CreateMunicipioDto] })
    create(@Res() Res, @Body() createMunicipioDto: CreateMunicipioDto) {
        this.municipiosService
            .create(createMunicipioDto)
            .then((municipio) => {
                return Res.status(HttpStatus.CREATED).json(municipio);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Get()
    async findAll(@Query('estadoId') estadoId: string) {
        return await this.municipiosService.findAll(estadoId);
    }

    @Get(':id')
    findOne(@Param('id') _id: string) {
        return this.municipiosService.findOne(_id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMunicipioDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateMunicipiooDto: UpdateMunicipioDto) {
        this.municipiosService
            .update(id, updateMunicipiooDto)
            .then((municipio) => {
                return Res.status(HttpStatus.ACCEPTED).json(municipio);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.municipiosService.remove(id);
    }
}
