import { IsNumber, IsString, IsUUID } from 'class-validator';
import { InstalacionesDeportiva } from '../../instalaciones-deportivas/entities/instalaciones-deportiva.entity';

export class CreateTechadoInstalacionDto {
    _id?: string;

    @IsString()
    nombreTechado: string;

    @IsNumber()
    medidaTechado: number;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;
}
