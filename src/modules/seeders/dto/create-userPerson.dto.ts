import { Persona } from 'src/modules/personas/entities/persona.entity';

export interface CreatePersonInterface {
    _id?: string;
    cedula: string;
    nombres: string;
    apellidos: string;
    telefono: string;
    tlfcasa: string;
    sexo: string;
    fechanac: Date;
    direccion: string;
}

export interface CreateUserInterface {
    usuario: string;
    contrasena: string;
    rol: string;
    isActive: boolean;
    persona?: Persona;
}
