import { Test, TestingModule } from '@nestjs/testing';
import { AtletasClubService } from './atletas-club.service';

describe('AtletasClubService', () => {
    let service: AtletasClubService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [AtletasClubService],
        }).compile();

        service = module.get<AtletasClubService>(AtletasClubService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
