import { Materiale } from '../../materiales/entities/materiale.entity';
import { EquipamientoDepotivoEsp } from '../../equipamiento-depotivo-esp/entities/equipamiento-depotivo-esp.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

@Entity()
export class MaterialEquipamientoEsp {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadMaterial: number;

    @ManyToOne(() => Materiale, (material) => material.equipamientoMaterial, { eager: true })
    material: Materiale;

    @ManyToOne(() => EquipamientoDepotivoEsp, (equipamiento) => equipamiento.equipamientoMaterial, { eager: true })
    equipamiento: EquipamientoDepotivoEsp;
}
