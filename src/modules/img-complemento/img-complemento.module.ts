import { Module } from '@nestjs/common';
import { ImgComplementoService } from './img-complemento.service';
import { ImgComplementoController } from './img-complemento.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgComplemento } from './entities/img-complemento.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgComplemento])],
    controllers: [ImgComplementoController],
    providers: [ImgComplementoService],
})
export class ImgComplementoModule {}
