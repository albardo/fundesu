import { Controller, Get, Post, Body, Param, Res, HttpStatus, Patch, Delete, Query } from '@nestjs/common';
import { ParroquiasService } from './parroquias.service';
import { CreateParroquiaDto } from './dto/create-parroquia.dto';
import { UpdateParroquiaDto } from './dto/update-parroquia.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('parroquias')
@ApiTags('Parroquias')
export class ParroquiasController {
    constructor(private readonly parroquiasService: ParroquiasService) {}

    @Post()
    @ApiBody({ type: [CreateParroquiaDto] })
    create(@Res() Res, @Body() createParroquiaDto: CreateParroquiaDto) {
        this.parroquiasService
            .create(createParroquiaDto)
            .then((parroquia) => {
                return Res.status(HttpStatus.CREATED).json(parroquia);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Get()
    async findAll(@Query('municipioId') municipioId: string) {
        return await this.parroquiasService.findAll(municipioId);
    }

    @Get(':id')
    findOne(@Param('id') _id: string) {
        return this.parroquiasService.findOne(_id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateParroquiaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateParroquiaDto: UpdateParroquiaDto) {
        this.parroquiasService
            .update(id, updateParroquiaDto)
            .then((municipio) => {
                return Res.status(HttpStatus.ACCEPTED).json(municipio);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.parroquiasService.remove(id);
    }
}
