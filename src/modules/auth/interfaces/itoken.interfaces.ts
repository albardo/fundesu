export interface IToken {
    getHash(): string;
    getUser(): any;
}
