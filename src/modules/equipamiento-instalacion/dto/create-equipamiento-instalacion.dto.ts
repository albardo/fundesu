import { EquipamientoDepotivoEsp } from '../../equipamiento-depotivo-esp/entities/equipamiento-depotivo-esp.entity';
import { InstalacionesDeportiva } from '../../instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { IsInt, IsUUID } from 'class-validator';

export class CreateEquipamientoInstalacionDto {
    _id?: string;

    @IsInt()
    cantidadEquipamiento: number;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;

    @IsUUID()
    equipamientoEsp: EquipamientoDepotivoEsp;
}
