import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgComplementoService } from './img-complemento.service';
import { CreateImgComplementoDto } from './dto/create-img-complemento.dto';
import { UpdateImgComplementoDto } from './dto/update-img-complemento.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-complemento')
@ApiTags('Img-complemento')
export class ImgComplementoController {
    constructor(private readonly imgComplementoService: ImgComplementoService) {}

    @Post()
    @ApiBody({ type: [CreateImgComplementoDto] })
    create(@Res() Res, @Body() createImgComplementoDto: CreateImgComplementoDto) {
        this.imgComplementoService.create(createImgComplementoDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgComplementoService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgComplementoService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgComplementoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgComplementoDto: UpdateImgComplementoDto) {
        this.imgComplementoService.update(id, updateImgComplementoDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgComplementoService.remove(id);
    }
}
