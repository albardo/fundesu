import { Test, TestingModule } from '@nestjs/testing';
import { ImgInstalacionesController } from './img-instalaciones.controller';
import { ImgInstalacionesService } from './img-instalaciones.service';

describe('ImgInstalacionesController', () => {
    let controller: ImgInstalacionesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgInstalacionesController],
            providers: [ImgInstalacionesService],
        }).compile();

        controller = module.get<ImgInstalacionesController>(ImgInstalacionesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
