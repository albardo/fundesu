import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImplementosDeportivoDto } from './dto/create-implementos-deportivo.dto';
import { UpdateImplementosDeportivoDto } from './dto/update-implementos-deportivo.dto';
import { ImplementosDeportivo } from './entities/implementos-deportivo.entity';

@Injectable()
export class ImplementosDeportivosService {
    constructor(
        @InjectRepository(ImplementosDeportivo)
        private readonly implementosRepository: Repository<ImplementosDeportivo>,
    ) {}
    async create(request: CreateImplementosDeportivoDto): Promise<ImplementosDeportivo> {
        const implementos = await this.implementosRepository.create(request);
        return await this.implementosRepository.save(implementos);
    }

    async findAll() {
        return await this.implementosRepository.find();
    }

    async findOne(id: string) {
        return await this.implementosRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateImplementosDeportivoDto): Promise<ImplementosDeportivo> {
        const upImplementos = await this.implementosRepository.findOne({ where: { _id: id } });
        await this.implementosRepository.merge(upImplementos, request);
        return await this.implementosRepository.save(upImplementos);
    }

    async remove(id: string) {
        return await this.implementosRepository.softDelete(id);
    }
}
