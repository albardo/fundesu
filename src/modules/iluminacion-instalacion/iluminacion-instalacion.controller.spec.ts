import { Test, TestingModule } from '@nestjs/testing';
import { IluminacionInstalacionController } from './iluminacion-instalacion.controller';
import { IluminacionInstalacionService } from './iluminacion-instalacion.service';

describe('IluminacionInstalacionController', () => {
    let controller: IluminacionInstalacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [IluminacionInstalacionController],
            providers: [IluminacionInstalacionService],
        }).compile();

        controller = module.get<IluminacionInstalacionController>(IluminacionInstalacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
