import { IsString } from 'class-validator';

export class CreateCargoDto {
    _id?: string;

    @IsString()
    nombreCargo: string;
}
