import { Module } from '@nestjs/common';
import { MiembrosClubesService } from './miembros-clubes.service';
import { MiembrosClubesController } from './miembros-clubes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MiembrosClube } from './entities/miembros-clube.entity';

@Module({
    imports: [TypeOrmModule.forFeature([MiembrosClube])],
    controllers: [MiembrosClubesController],
    providers: [MiembrosClubesService],
})
export class MiembrosClubesModule {}
