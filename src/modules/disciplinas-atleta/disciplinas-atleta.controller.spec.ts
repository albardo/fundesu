import { Test, TestingModule } from '@nestjs/testing';
import { DisciplinasAtletaController } from './disciplinas-atleta.controller';
import { DisciplinasAtletaService } from './disciplinas-atleta.service';

describe('DisciplinasAtletaController', () => {
    let controller: DisciplinasAtletaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [DisciplinasAtletaController],
            providers: [DisciplinasAtletaService],
        }).compile();

        controller = module.get<DisciplinasAtletaController>(DisciplinasAtletaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
