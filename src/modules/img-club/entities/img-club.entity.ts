import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImgClub {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'text', nullable: true })
    image: string;

    @ManyToOne(() => Clube, (club) => club.imageClub, { eager: true })
    club: Clube;
}
