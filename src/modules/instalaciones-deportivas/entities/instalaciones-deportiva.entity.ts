import { ComplejosDeportivo } from 'src/modules/complejos-deportivos/entities/complejos-deportivo.entity';
import { ComplementosInstalacion } from 'src/modules/complementos-instalacion/entities/complementos-instalacion.entity';
import { EquipamientoInstalacion } from 'src/modules/equipamiento-instalacion/entities/equipamiento-instalacion.entity';
import { IluminacionInstalacion } from 'src/modules/iluminacion-instalacion/entities/iluminacion-instalacion.entity';
import { ImgComplemento } from 'src/modules/img-complemento/entities/img-complemento.entity';
import { ImgIluminacion } from 'src/modules/img-iluminacion/entities/img-iluminacion.entity';
import { ImgInstalacione } from 'src/modules/img-instalaciones/entities/img-instalacione.entity';
import { ImgPared } from 'src/modules/img-pared/entities/img-pared.entity';
import { ImgSuperficie } from 'src/modules/img-superficie/entities/img-superficie.entity';
import { ImgTechado } from 'src/modules/img-techado/entities/img-techado.entity';
import { ImplementosInstalacion } from 'src/modules/implementos-instalacion/entities/implementos-instalacion.entity';
import { ParedInstalacion } from 'src/modules/pared-instalacion/entities/pared-instalacion.entity';
import { SuperficieInstalacion } from 'src/modules/superficie-instalacion/entities/superficie-instalacion.entity';
import { TechadoInstalacion } from 'src/modules/techado-instalacion/entities/techado-instalacion.entity';
import { TipoInstalacionDep } from 'src/modules/tipo-instalacion-dep/entities/tipo-instalacion-dep.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class InstalacionesDeportiva {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreInstalacion: string;

    @Column({ type: 'float' })
    medidaLargo: number;

    @Column({ type: 'float' })
    medidaAncho: number;

    @Column({ type: 'float' })
    metrosCuadrados: number;

    @Column({ type: 'varchar' })
    tipoEspacio: string;

    @Column({ type: 'varchar' })
    estadoInstalacion: string;

    @Column({ type: 'boolean' })
    recuperacionInstalacion: boolean;

    @ManyToOne(() => ComplejosDeportivo, (complejoDeportivo) => complejoDeportivo.instalacionDeportiva, { eager: true })
    complejosDeportivos: ComplejosDeportivo;

    @ManyToOne(() => TipoInstalacionDep, (tipoInstalacionDep) => tipoInstalacionDep.instalacionDeportiva, {
        eager: true,
    })
    tipoInstalacionDep: TipoInstalacionDep;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => SuperficieInstalacion, (superficie) => superficie.instalaciones)
    superficie: SuperficieInstalacion;

    @OneToMany(() => ParedInstalacion, (pared) => pared.instalaciones)
    pared: ParedInstalacion;

    @OneToMany(() => IluminacionInstalacion, (iluminacion) => iluminacion.instalaciones)
    iluminacion: IluminacionInstalacion;

    @OneToMany(() => TechadoInstalacion, (techado) => techado.instalaciones)
    techado: TechadoInstalacion;

    @OneToMany(() => ComplementosInstalacion, (complementos) => complementos.instalaciones)
    complementos: ComplementosInstalacion;

    @OneToMany(() => EquipamientoInstalacion, (equipamiento) => equipamiento.instalaciones)
    equipamiento: EquipamientoInstalacion;

    @OneToMany(() => ImplementosInstalacion, (implementos) => implementos.instalaciones)
    implementos: ImplementosInstalacion;

    @OneToMany(() => ImgInstalacione, (imageInstalaciones) => imageInstalaciones.instalaciones)
    imageInstalaciones: ImgInstalacione;

    @OneToMany(() => ImgComplemento, (imageComplemento) => imageComplemento.instalaciones)
    imageComplemento: ImgComplemento;

    @OneToMany(() => ImgIluminacion, (imageIluminacion) => imageIluminacion.instalaciones)
    imageIluminacion: ImgIluminacion;

    @OneToMany(() => ImgPared, (imagePared) => imagePared.instalaciones)
    imagePared: ImgPared;

    @OneToMany(() => ImgSuperficie, (imageSuperficie) => imageSuperficie.instalaciones)
    imageSuperficie: ImgSuperficie;

    @OneToMany(() => ImgTechado, (imageTechado) => imageTechado.instalaciones)
    imageTechado: ImgTechado;
}
