import { Module } from '@nestjs/common';
import { ComunidadesService } from './comunidades.service';
import { ComunidadesController } from './comunidades.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comunidades } from './entities/comunidade.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { Notification } from '../notification/entities/notification.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Comunidades, Notification, Usuario])],
    controllers: [ComunidadesController],
    providers: [ComunidadesService],
})
export class ComunidadesModule {}
