import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { EquipamientoInstalacionService } from './equipamiento-instalacion.service';
import { CreateEquipamientoInstalacionDto } from './dto/create-equipamiento-instalacion.dto';
import { UpdateEquipamientoInstalacionDto } from './dto/update-equipamiento-instalacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('equipamiento-instalacion')
@ApiTags('Equipamiento-instalacion')
export class EquipamientoInstalacionController {
    constructor(private readonly equipamientoInstalacionService: EquipamientoInstalacionService) {}

    @Post()
    @ApiBody({ type: [CreateEquipamientoInstalacionDto] })
    create(@Res() Res, @Body() createEquipamientoInstalacionDto: CreateEquipamientoInstalacionDto) {
        this.equipamientoInstalacionService
            .create(createEquipamientoInstalacionDto)
            .then((equipamiento) => {
                return Res.status(HttpStatus.CREATED).json(equipamiento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.equipamientoInstalacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.equipamientoInstalacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateEquipamientoInstalacionDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateEquipamientoInstalacionDto: UpdateEquipamientoInstalacionDto,
    ) {
        this.equipamientoInstalacionService
            .update(id, updateEquipamientoInstalacionDto)
            .then((equipamiento) => {
                return Res.status(HttpStatus.ACCEPTED).json(equipamiento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.equipamientoInstalacionService.remove(id);
    }
}
