import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EstadosModule } from './modules/estados/estados.module';
import { MunicipiosModule } from './modules/municipios/municipios.module';
import { ParroquiasModule } from './modules/parroquias/parroquias.module';
import { SectoresModule } from './modules/sectores/sectores.module';
import { PersonasModule } from './modules/personas/personas.module';
import { UsuariosModule } from './modules/usuarios/usuarios.module';
import { AtletasModule } from './modules/atletas/atletas.module';
import { ComplejosDeportivosModule } from './modules/complejos-deportivos/complejos-deportivos.module';
import { DisciplinasDeportivasModule } from './modules/disciplinas-deportivas/disciplinas-deportivas.module';
import { DisciplinasAtletaModule } from './modules/disciplinas-atleta/disciplinas-atleta.module';
import { TallaAtletaModule } from './modules/talla-atleta/talla-atleta.module';
import { AlturaPesoAtletaModule } from './modules/altura-peso-atleta/altura-peso-atleta.module';
import { TipoAtletaModule } from './modules/tipo-atleta/tipo-atleta.module';
import { InstalacionesDeportivasModule } from './modules/instalaciones-deportivas/instalaciones-deportivas.module';
import { TipoInstalacionDepModule } from './modules/tipo-instalacion-dep/tipo-instalacion-dep.module';
import { MaterialesModule } from './modules/materiales/materiales.module';
import { ImplementosDeportivosModule } from './modules/implementos-deportivos/implementos-deportivos.module';
import { EquipamientoDepotivoEspModule } from './modules/equipamiento-depotivo-esp/equipamiento-depotivo-esp.module';
import { SuperficieInstalacionModule } from './modules/superficie-instalacion/superficie-instalacion.module';
import { ParedInstalacionModule } from './modules/pared-instalacion/pared-instalacion.module';
import { IluminacionInstalacionModule } from './modules/iluminacion-instalacion/iluminacion-instalacion.module';
import { TechadoInstalacionModule } from './modules/techado-instalacion/techado-instalacion.module';
import { ComplementosInstalacionModule } from './modules/complementos-instalacion/complementos-instalacion.module';
import { EquipamientoInstalacionModule } from './modules/equipamiento-instalacion/equipamiento-instalacion.module';
import { ImplementosInstalacionModule } from './modules/implementos-instalacion/implementos-instalacion.module';
import { ComunidadesModule } from './modules/comunidades/comunidades.module';
import { DiscapacidadesModule } from './modules/discapacidades/discapacidades.module';
import { BecasModule } from './modules/becas/becas.module';
import { DiscapacidadesAtletaModule } from './modules/discapacidades-atleta/discapacidades-atleta.module';
import { BecasAtletasModule } from './modules/becas-atletas/becas-atletas.module';
import { MaterialSuperficieModule } from './modules/material-superficie/material-superficie.module';
import { MaterialParedModule } from './modules/material-pared/material-pared.module';
import { MaterialIluminacionModule } from './modules/material-iluminacion/material-iluminacion.module';
import { MaterialComplementoModule } from './modules/material-complemento/material-complemento.module';
import { MaterialEquipamientoEspModule } from './modules/material-equipamiento-esp/material-equipamiento-esp.module';
import { MaterialTechadoModule } from './modules/material-techado/material-techado.module';
import { ClubesModule } from './modules/clubes/clubes.module';
import { CargosModule } from './modules/cargos/cargos.module';
import { MiembrosClubesModule } from './modules/miembros-clubes/miembros-clubes.module';
import { ComplejosClubesModule } from './modules/complejos-clubes/complejos-clubes.module';
import { EventosModule } from './modules/eventos/eventos.module';
import { EventosAtletasModule } from './modules/eventos-atletas/eventos-atletas.module';
import { AtletasClubModule } from './modules/atletas-club/atletas-club.module';
import { InstalacionDisciplinaModule } from './modules/instalacion-disciplina/instalacion-disciplina.module';
import { ImgSuperficieModule } from './modules/img-superficie/img-superficie.module';
import { ImgParedModule } from './modules/img-pared/img-pared.module';
import { ImgIluminacionModule } from './modules/img-iluminacion/img-iluminacion.module';
import { ImgTechadoModule } from './modules/img-techado/img-techado.module';
import { ImgComplementoModule } from './modules/img-complemento/img-complemento.module';
import { ImgClubModule } from './modules/img-club/img-club.module';
import { ImgComplejoDepModule } from './modules/img-complejo-dep/img-complejo-dep.module';
import { ImgPersonaModule } from './modules/img-persona/img-persona.module';
import { ImgInstalacionesModule } from './modules/img-instalaciones/img-instalaciones.module';
import { connection } from './config/ormconfig.config';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './modules/auth/auth.module';
import { NotificationModule } from './modules/notification/notification.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { ScheduleModule } from '@nestjs/schedule';
@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        ScheduleModule.forRoot(),
        connection,
        EstadosModule,
        MunicipiosModule,
        ParroquiasModule,
        SectoresModule,
        PersonasModule,
        UsuariosModule,
        AtletasModule,
        ComplejosDeportivosModule,
        DisciplinasDeportivasModule,
        DisciplinasAtletaModule,
        TallaAtletaModule,
        AlturaPesoAtletaModule,
        TipoAtletaModule,
        InstalacionesDeportivasModule,
        TipoInstalacionDepModule,
        MaterialesModule,
        ImplementosDeportivosModule,
        EquipamientoDepotivoEspModule,
        SuperficieInstalacionModule,
        ParedInstalacionModule,
        IluminacionInstalacionModule,
        TechadoInstalacionModule,
        ComplementosInstalacionModule,
        EquipamientoInstalacionModule,
        ImplementosInstalacionModule,
        ComunidadesModule,
        DiscapacidadesModule,
        BecasModule,
        DiscapacidadesAtletaModule,
        BecasAtletasModule,
        MaterialSuperficieModule,
        MaterialParedModule,
        MaterialIluminacionModule,
        MaterialComplementoModule,
        MaterialEquipamientoEspModule,
        MaterialTechadoModule,
        ClubesModule,
        CargosModule,
        MiembrosClubesModule,
        ComplejosClubesModule,
        EventosModule,
        EventosAtletasModule,
        AtletasClubModule,
        InstalacionDisciplinaModule,
        ImgSuperficieModule,
        ImgParedModule,
        ImgIluminacionModule,
        ImgTechadoModule,
        ImgComplementoModule,
        ImgClubModule,
        ImgComplejoDepModule,
        ImgPersonaModule,
        ImgInstalacionesModule,
        AuthModule,
        DashboardModule,
        NotificationModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
