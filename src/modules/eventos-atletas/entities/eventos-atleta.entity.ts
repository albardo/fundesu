import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { DisciplinasAtleta } from 'src/modules/disciplinas-atleta/entities/disciplinas-atleta.entity';
import { Evento } from 'src/modules/eventos/entities/evento.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class EventosAtleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @ManyToOne(() => Atleta, (atleta) => atleta.evento, { eager: true })
    atleta: Atleta;

    @ManyToOne(() => Evento, (evento) => evento.evento, { eager: true })
    evento: Evento;

    @Column({ type: 'integer' })
    posicion: number;
}
