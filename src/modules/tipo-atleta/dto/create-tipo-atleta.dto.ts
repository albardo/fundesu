import { IsString } from 'class-validator';

export class CreateTipoAtletaDto {
    _id?: string;

    @IsString()
    nombreTipoAtleta: string;
}
