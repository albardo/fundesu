import { PartialType } from '@nestjs/mapped-types';
import { CreateImgComplementoDto } from './create-img-complemento.dto';

export class UpdateImgComplementoDto extends PartialType(CreateImgComplementoDto) {}
