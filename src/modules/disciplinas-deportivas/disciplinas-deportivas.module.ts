import { Module } from '@nestjs/common';
import { DisciplinasDeportivasService } from './disciplinas-deportivas.service';
import { DisciplinasDeportivasController } from './disciplinas-deportivas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DisciplinasDeportiva } from './entities/disciplinas-deportiva.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([DisciplinasDeportiva, Notification, Usuario])],
    controllers: [DisciplinasDeportivasController],
    providers: [DisciplinasDeportivasService],
})
export class DisciplinasDeportivasModule {}
