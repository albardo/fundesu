import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res, Request } from '@nestjs/common';
import { MaterialesService } from './materiales.service';
import { CreateMaterialeDto } from './dto/create-materiale.dto';
import { UpdateMaterialeDto } from './dto/update-materiale.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';

@Controller('materiales')
@ApiTags('Materiales')
export class MaterialesController {
    constructor(private readonly materialesService: MaterialesService) {}

    @AuthAll()
    @Post('/')
    @ApiBody({ type: [CreateMaterialeDto] })
    create(@Request() req, @Res() Res, @Body() createMaterialeDto: CreateMaterialeDto) {
        this.materialesService
            .create(createMaterialeDto, req.user)
            .then((material) => {
                return Res.status(HttpStatus.CREATED).json(material);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.materialesService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.materialesService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMaterialeDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateMaterialeDto: UpdateMaterialeDto) {
        this.materialesService
            .update(id, updateMaterialeDto)
            .then((material) => {
                return Res.status(HttpStatus.CREATED).json(material);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.materialesService.remove(id, req.user);
    }
}
