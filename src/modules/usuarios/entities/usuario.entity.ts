import { hash, genSalt, compareSync } from 'bcrypt';
import { Exclude } from 'class-transformer';
import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { Municipio } from 'src/modules/municipios/entities/municipio.entity';
import { Notification } from 'src/modules/notification/entities/notification.entity';
import { Persona } from 'src/modules/personas/entities/persona.entity';
import {
    BeforeInsert,
    BeforeUpdate,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Usuario {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ unique: true })
    usuario: string;

    @Column({ type: 'varchar' })
    @Exclude()
    contrasena: string;

    @Column({ type: 'varchar' })
    rol: string;

    @Column({ default: true })
    isActive: boolean;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ nullable: true })
    @DeleteDateColumn()
    deleteAt?: Date;

    @OneToOne(() => Persona, (persona) => persona.usuario, { eager: true })
    @JoinColumn({ name: 'persona_id' })
    persona: Persona;

    @ManyToOne(() => Municipio, (municipio) => municipio.usuario, { eager: true, nullable: true })
    municipio: Municipio;

    @ManyToOne(() => Clube, (club) => club.usuario, { eager: true, nullable: true })
    club: Clube;

    @OneToMany(() => Notification, (notification) => notification.user)
    notification: Notification[];

    @BeforeInsert()
    @BeforeUpdate()
    async hashPassword() {
        this.contrasena = await Usuario.setHashPassword(this.contrasena);
    }

    static async setHashPassword(password: string) {
        const salt = await genSalt();
        return await hash(password, salt);
    }

    async validatePassword(password: string): Promise<boolean> {
        return compareSync(password, this.contrasena);
    }
}
