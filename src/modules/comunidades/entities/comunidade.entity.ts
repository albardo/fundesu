import { Parroquia } from 'src/modules/parroquias/entities/parroquia.entity';
import { Sector } from 'src/modules/sectores/entities/sectore.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Comunidades {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreComunidad: string;

    @ManyToOne(() => Parroquia, (parroquia) => parroquia.comunidad, { eager: true })
    parroquia: Parroquia;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => Sector, (sector) => sector.comunidad)
    sector: Sector;
}
