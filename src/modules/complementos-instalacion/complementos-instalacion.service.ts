import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateComplementosInstalacionDto } from './dto/create-complementos-instalacion.dto';
import { UpdateComplementosInstalacionDto } from './dto/update-complementos-instalacion.dto';
import { ComplementosInstalacion } from './entities/complementos-instalacion.entity';

@Injectable()
export class ComplementosInstalacionService {
    constructor(
        @InjectRepository(ComplementosInstalacion)
        private readonly complementoInstRepository: Repository<ComplementosInstalacion>,
    ) {}

    async create(request: CreateComplementosInstalacionDto) {
        const complemento = this.complementoInstRepository.create(request);
        return await this.complementoInstRepository.save(complemento);
    }

    async findAll() {
        return await this.complementoInstRepository.find();
    }

    async findOne(id: string) {
        const complemento = this.complementoInstRepository
            .createQueryBuilder('i')
            .where('instalaciones_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateComplementosInstalacionDto) {
        const upComplemento = await this.complementoInstRepository.findOne({ where: { _id: id } });
        upComplemento.instalaciones = request.instalaciones;
        this.complementoInstRepository.merge(upComplemento, request);
        return await this.complementoInstRepository.save(upComplemento);
    }

    async remove(id: string) {
        return await this.complementoInstRepository.delete(id);
    }
}
