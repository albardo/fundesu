import { Test, TestingModule } from '@nestjs/testing';
import { MaterialIluminacionController } from './material-iluminacion.controller';
import { MaterialIluminacionService } from './material-iluminacion.service';

describe('MaterialIluminacionController', () => {
    let controller: MaterialIluminacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MaterialIluminacionController],
            providers: [MaterialIluminacionService],
        }).compile();

        controller = module.get<MaterialIluminacionController>(MaterialIluminacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
