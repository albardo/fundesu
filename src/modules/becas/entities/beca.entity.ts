import { BecasAtleta } from 'src/modules/becas-atletas/entities/becas-atleta.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Beca {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreBeca: string;

    @Column({ type: 'float' })
    monto: number;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => BecasAtleta, (becaAtleta) => becaAtleta.beca)
    becaAtleta: BecasAtleta;
}
