import { Test, TestingModule } from '@nestjs/testing';
import { MaterialTechadoController } from './material-techado.controller';
import { MaterialTechadoService } from './material-techado.service';

describe('MaterialTechadoController', () => {
    let controller: MaterialTechadoController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MaterialTechadoController],
            providers: [MaterialTechadoService],
        }).compile();

        controller = module.get<MaterialTechadoController>(MaterialTechadoController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
