import { IsString } from 'class-validator';

export class CreateMaterialeDto {
    _id?: string;

    @IsString()
    nombreMaterial: string;
}
