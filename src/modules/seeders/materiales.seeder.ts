import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { Materiale } from '../materiales/entities/materiale.entity';


@Injectable()
export class MaterialSeeder implements Seeder {
    constructor(
        @InjectRepository(Materiale)
        private readonly materialesRepository: Repository<Materiale>,
    ) {}

    async seed(): Promise<any> {
        const { materials } = JSON.parse(readFileSync('./src/modules/seeders/jsons/materials.json', 'utf8'));

        try {
            for await (const material of materials) {
                await this.materialesRepository.save({ nombreMaterial: material });
            }
        } catch (exception: any) {
            console.log('Crear tipo instalacion deportiva: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const material = await this.materialesRepository.find();
        return await this.materialesRepository.remove(material);
    }
}
