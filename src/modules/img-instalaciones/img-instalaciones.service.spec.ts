import { Test, TestingModule } from '@nestjs/testing';
import { ImgInstalacionesService } from './img-instalaciones.service';

describe('ImgInstalacionesService', () => {
    let service: ImgInstalacionesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgInstalacionesService],
        }).compile();

        service = module.get<ImgInstalacionesService>(ImgInstalacionesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
