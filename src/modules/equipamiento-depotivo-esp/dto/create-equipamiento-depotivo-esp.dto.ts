import { IsString } from 'class-validator';

export class CreateEquipamientoDepotivoEspDto {
    _id?: string;

    @IsString()
    nombreEquipamiento: string;
}
