import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { MaterialEquipamientoEspService } from './material-equipamiento-esp.service';
import { CreateMaterialEquipamientoEspDto } from './dto/create-material-equipamiento-esp.dto';
import { UpdateMaterialEquipamientoEspDto } from './dto/update-material-equipamiento-esp.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('material-equipamiento-esp')
@ApiTags('Material-equipamiento-especial')
export class MaterialEquipamientoEspController {
    constructor(private readonly materialEquipamientoEspService: MaterialEquipamientoEspService) {}

    @Post()
    @ApiBody({ type: [CreateMaterialEquipamientoEspDto] })
    create(@Res() Res, @Body() createMaterialEquipamientoEspDto: CreateMaterialEquipamientoEspDto) {
        this.materialEquipamientoEspService
            .create(createMaterialEquipamientoEspDto)
            .then((equipamiento) => {
                return Res.status(HttpStatus.CREATED).json(equipamiento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.materialEquipamientoEspService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.materialEquipamientoEspService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMaterialEquipamientoEspDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateMaterialEquipamientoEspDto: UpdateMaterialEquipamientoEspDto,
    ) {
        this.materialEquipamientoEspService
            .update(id, updateMaterialEquipamientoEspDto)
            .then((equipamiento) => {
                return Res.status(HttpStatus.ACCEPTED).json(equipamiento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.materialEquipamientoEspService.remove(id);
    }
}
