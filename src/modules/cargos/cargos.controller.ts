import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { CargosService } from './cargos.service';
import { CreateCargoDto } from './dto/create-cargo.dto';
import { UpdateCargoDto } from './dto/update-cargo.dto';

@Controller('cargos')
@ApiTags('Cargos')
export class CargosController {
    constructor(private readonly cargosService: CargosService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateCargoDto] })
    create(@Request() req, @Res() Res, @Body() createCargoDto: CreateCargoDto) {
        return this.cargosService
            .create(createCargoDto, req.user)
            .then((cargo) => {
                return Res.status(HttpStatus.CREATED).json(cargo);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.cargosService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.cargosService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateCargoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateCargoDto: UpdateCargoDto) {
        return this.cargosService
            .update(id, updateCargoDto)
            .then((cargo) => {
                return Res.status(HttpStatus.ACCEPTED).json(cargo);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.cargosService.remove(id, req.user);
    }
}
