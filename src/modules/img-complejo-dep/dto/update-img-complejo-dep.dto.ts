import { PartialType } from '@nestjs/mapped-types';
import { CreateImgComplejoDepDto } from './create-img-complejo-dep.dto';

export class UpdateImgComplejoDepDto extends PartialType(CreateImgComplejoDepDto) {}
