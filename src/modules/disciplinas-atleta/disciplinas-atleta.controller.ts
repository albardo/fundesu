import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { DisciplinasAtletaService } from './disciplinas-atleta.service';
import { CreateDisciplinasAtletaDto } from './dto/create-disciplinas-atleta.dto';
import { UpdateDisciplinasAtletaDto } from './dto/update-disciplinas-atleta.dto';

@Controller('disciplinas-atleta')
@ApiTags('Disciplinas-atleta')
export class DisciplinasAtletaController {
    constructor(private readonly disciplinasAtletaService: DisciplinasAtletaService) {}

    @Post()
    @ApiBody({ type: [CreateDisciplinasAtletaDto] })
    create(@Res() Res, @Body() createDisciplinasAtletaDto: CreateDisciplinasAtletaDto) {
        this.disciplinasAtletaService
            .create(createDisciplinasAtletaDto)
            .then((disciplinas) => {
                return Res.status(HttpStatus.CREATED).json(disciplinas);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.disciplinasAtletaService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.disciplinasAtletaService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateDisciplinasAtletaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateDisciplinasAtletaDto: UpdateDisciplinasAtletaDto) {
        return this.disciplinasAtletaService
            .update(id, updateDisciplinasAtletaDto)
            .then((disciplinas) => {
                return Res.status(HttpStatus.ACCEPTED).json(disciplinas);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.disciplinasAtletaService.remove(id);
    }
}
