import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { Comunidades } from 'src/modules/comunidades/entities/comunidade.entity';

export class CreateSectoreDto {
    _id?: string;

    @IsNotEmpty()
    @IsString()
    nombreSector: string;

    @IsUUID()
    comunidad: Comunidades;
}
