import { Module } from '@nestjs/common';
import { MaterialTechadoService } from './material-techado.service';
import { MaterialTechadoController } from './material-techado.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MaterialTechado } from './entities/material-techado.entity';
import { Materiale } from '../materiales/entities/materiale.entity';

@Module({
    imports: [TypeOrmModule.forFeature([MaterialTechado, Materiale])],
    controllers: [MaterialTechadoController],
    providers: [MaterialTechadoService],
})
export class MaterialTechadoModule {}
