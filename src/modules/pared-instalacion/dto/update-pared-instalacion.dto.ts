import { PartialType } from '@nestjs/mapped-types';
import { CreateParedInstalacionDto } from './create-pared-instalacion.dto';

export class UpdateParedInstalacionDto extends PartialType(CreateParedInstalacionDto) {}
