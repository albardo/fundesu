import { IsInt, IsUUID } from 'class-validator';
import { Materiale } from '../../materiales/entities/materiale.entity';
import { SuperficieInstalacion } from '../../superficie-instalacion/entities/superficie-instalacion.entity';

export class CreateMaterialSuperficieDto {
    _id?: string;

    @IsInt()
    cantidadMaterial: number;

    @IsUUID()
    material: Materiale;

    @IsUUID()
    superficie: SuperficieInstalacion;
}
