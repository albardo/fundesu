import { Test, TestingModule } from '@nestjs/testing';
import { AtletasClubController } from './atletas-club.controller';
import { AtletasClubService } from './atletas-club.service';

describe('AtletasClubController', () => {
    let controller: AtletasClubController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AtletasClubController],
            providers: [AtletasClubService],
        }).compile();

        controller = module.get<AtletasClubController>(AtletasClubController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
