import { IsInt, IsUUID } from 'class-validator';
import { IluminacionInstalacion } from 'src/modules/iluminacion-instalacion/entities/iluminacion-instalacion.entity';
import { Materiale } from 'src/modules/materiales/entities/materiale.entity';

export class CreateMaterialIluminacionDto {
    _id?: string;

    @IsInt()
    cantidadMaterial: number;

    @IsUUID()
    material: Materiale;

    @IsUUID()
    iluminacion: IluminacionInstalacion;
}
