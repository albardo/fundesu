import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgTechadoService } from './img-techado.service';
import { CreateImgTechadoDto } from './dto/create-img-techado.dto';
import { UpdateImgTechadoDto } from './dto/update-img-techado.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-techado')
@ApiTags('Img-techado')
export class ImgTechadoController {
    constructor(private readonly imgTechadoService: ImgTechadoService) {}

    @Post()
    @ApiBody({ type: [CreateImgTechadoDto] })
    create(@Res() Res, @Body() createImgTechadoDto: CreateImgTechadoDto) {
        this.imgTechadoService.create(createImgTechadoDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgTechadoService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgTechadoService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgTechadoDto] })
    update(@Param('id') id: string, @Res() Res, @Body() updateImgTechadoDto: UpdateImgTechadoDto) {
        this.imgTechadoService.update(id, updateImgTechadoDto).then((img) => {
            return Res.status(HttpStatus.ACCEPTED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgTechadoService.remove(id);
    }
}
