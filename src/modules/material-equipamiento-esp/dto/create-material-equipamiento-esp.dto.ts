import { IsInt, IsUUID } from 'class-validator';
import { EquipamientoDepotivoEsp } from 'src/modules/equipamiento-depotivo-esp/entities/equipamiento-depotivo-esp.entity';
import { Materiale } from 'src/modules/materiales/entities/materiale.entity';

export class CreateMaterialEquipamientoEspDto {
    id?: string;

    @IsInt()
    cantidadMaterial: number;

    @IsUUID()
    material: Materiale;

    @IsUUID()
    equipamiento: EquipamientoDepotivoEsp;
}
