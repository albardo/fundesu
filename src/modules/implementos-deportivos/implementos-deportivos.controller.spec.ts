import { Test, TestingModule } from '@nestjs/testing';
import { ImplementosDeportivosController } from './implementos-deportivos.controller';
import { ImplementosDeportivosService } from './implementos-deportivos.service';

describe('ImplementosDeportivosController', () => {
    let controller: ImplementosDeportivosController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImplementosDeportivosController],
            providers: [ImplementosDeportivosService],
        }).compile();

        controller = module.get<ImplementosDeportivosController>(ImplementosDeportivosController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
