import { TypeOrmModule } from '@nestjs/typeorm';
import { seeder } from 'nestjs-seeder';
import { connection } from './config/ormconfig.config';
import { Beca } from './modules/becas/entities/beca.entity';
import { Cargo } from './modules/cargos/entities/cargo.entity';
import { Discapacidade } from './modules/discapacidades/entities/discapacidade.entity';
import { DisciplinasDeportiva } from './modules/disciplinas-deportivas/entities/disciplinas-deportiva.entity';
import { EquipamientoDepotivoEsp } from './modules/equipamiento-depotivo-esp/entities/equipamiento-depotivo-esp.entity';
import { Estado } from './modules/estados/entities/estado.entity';
import { Evento } from './modules/eventos/entities/evento.entity';
import { ImplementosDeportivo } from './modules/implementos-deportivos/entities/implementos-deportivo.entity';
import { Materiale } from './modules/materiales/entities/materiale.entity';
import { Municipio } from './modules/municipios/entities/municipio.entity';
import { Parroquia } from './modules/parroquias/entities/parroquia.entity';
import { Persona } from './modules/personas/entities/persona.entity';
import { BecasSeeder } from './modules/seeders/becas.seeder';
import { CargosSeeder } from './modules/seeders/cargos.seeder';
import { DiscapacidadesSeeder } from './modules/seeders/discapacidades.seeder';
import { EquipamientoDepotivoEspSeeder } from './modules/seeders/equipamientosDeportivos.seeder';
import { EventosSeeder } from './modules/seeders/eventos.seeder';
import { ImplementosDeportivosSeeder } from './modules/seeders/implementosDeportivos.seeder';
import { TypeInstSeeder } from './modules/seeders/instTypes.seeder';
import { LocationsSeeder } from './modules/seeders/locations.seeder';
import { MaterialSeeder } from './modules/seeders/materiales.seeder';
import { SportsSeeder } from './modules/seeders/sports.seeder';
import { TipoAtletaSeeder } from './modules/seeders/tiposAtletas.seeder';
import { UserSeeder } from './modules/seeders/user.seeder';
import { TipoAtleta } from './modules/tipo-atleta/entities/tipo-atleta.entity';
import { TipoInstalacionDep } from './modules/tipo-instalacion-dep/entities/tipo-instalacion-dep.entity';
import { Usuario } from './modules/usuarios/entities/usuario.entity';


seeder({
    imports: [
        connection,
        TypeOrmModule.forFeature([
            Usuario, 
            Persona, 
            Estado, 
            Municipio, 
            Parroquia, 
            DisciplinasDeportiva, 
            TipoInstalacionDep, 
            Beca, 
            Evento, 
            ImplementosDeportivo, 
            Materiale, 
            TipoAtleta,
            Cargo,
            Discapacidade,
            EquipamientoDepotivoEsp
        ]),
    ],
}).run([
    UserSeeder, 
    LocationsSeeder, 
    SportsSeeder, 
    TypeInstSeeder, 
    CargosSeeder, 
    EventosSeeder, 
    ImplementosDeportivosSeeder, 
    MaterialSeeder,
    TipoAtletaSeeder,
    DiscapacidadesSeeder,
    BecasSeeder,
    EquipamientoDepotivoEspSeeder
]);
