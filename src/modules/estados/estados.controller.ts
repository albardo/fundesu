import { Controller, Get, Post, Body, Param, Res, HttpStatus, Patch, Delete } from '@nestjs/common';
import { EstadosService } from './estados.service';
import { CreateEstadoDto } from './dto/create-estado.dto';
import { UpdateEstadoDto } from './dto/update-estado.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('estados')
@ApiTags('Estados')
export class EstadosController {
    constructor(private readonly estadosService: EstadosService) {}

    @Post('/')
    @ApiBody({ type: [CreateEstadoDto] })
    create(@Res() Res, @Body() createEstadoDto: CreateEstadoDto) {
        this.estadosService
            .create(createEstadoDto)
            .then((estados) => {
                return Res.status(HttpStatus.CREATED).json(estados);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Get()
    findAll() {
        return this.estadosService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.estadosService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateEstadoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateEstadoDto: UpdateEstadoDto) {
        this.estadosService
            .update(id, updateEstadoDto)
            .then((cargo) => {
                return Res.status(HttpStatus.ACCEPTED).json(cargo);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.estadosService.remove(id);
    }
}
