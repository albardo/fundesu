import { Test, TestingModule } from '@nestjs/testing';
import { EquipamientoInstalacionService } from './equipamiento-instalacion.service';

describe('EquipamientoInstalacionService', () => {
    let service: EquipamientoInstalacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [EquipamientoInstalacionService],
        }).compile();

        service = module.get<EquipamientoInstalacionService>(EquipamientoInstalacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
