import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { EquipamientoDepotivoEspService } from './equipamiento-depotivo-esp.service';
import { CreateEquipamientoDepotivoEspDto } from './dto/create-equipamiento-depotivo-esp.dto';
import { UpdateEquipamientoDepotivoEspDto } from './dto/update-equipamiento-depotivo-esp.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('equipamiento-depotivo-esp')
@ApiTags('Equipamiento-deportivos-especifico')
export class EquipamientoDepotivoEspController {
    constructor(private readonly equipamientoDepotivoEspService: EquipamientoDepotivoEspService) {}

    @Post()
    @ApiBody({ type: [CreateEquipamientoDepotivoEspDto] })
    create(@Res() Res, @Body() createEquipamientoDepotivoEspDto: CreateEquipamientoDepotivoEspDto) {
        this.equipamientoDepotivoEspService
            .create(createEquipamientoDepotivoEspDto)
            .then((equipamiento) => {
                return Res.status(HttpStatus.CREATED).json(equipamiento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.equipamientoDepotivoEspService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.equipamientoDepotivoEspService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateEquipamientoDepotivoEspDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateEquipamientoDepotivoEspDto: UpdateEquipamientoDepotivoEspDto,
    ) {
        this.equipamientoDepotivoEspService
            .update(id, updateEquipamientoDepotivoEspDto)
            .then((equipamiento) => {
                return Res.status(HttpStatus.CREATED).json(equipamiento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.equipamientoDepotivoEspService.remove(id);
    }
}
