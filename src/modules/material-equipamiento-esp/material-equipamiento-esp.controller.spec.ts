import { Test, TestingModule } from '@nestjs/testing';
import { MaterialEquipamientoEspController } from './material-equipamiento-esp.controller';
import { MaterialEquipamientoEspService } from './material-equipamiento-esp.service';

describe('MaterialEquipamientoEspController', () => {
    let controller: MaterialEquipamientoEspController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MaterialEquipamientoEspController],
            providers: [MaterialEquipamientoEspService],
        }).compile();

        controller = module.get<MaterialEquipamientoEspController>(MaterialEquipamientoEspController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
