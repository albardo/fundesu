import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Beca } from '../becas/entities/beca.entity';
import { CreateBecasAtletaDto } from './dto/create-becas-atleta.dto';
import { UpdateBecasAtletaDto } from './dto/update-becas-atleta.dto';
import { BecasAtleta } from './entities/becas-atleta.entity';

@Injectable()
export class BecasAtletasService {
    constructor(
        @InjectRepository(BecasAtleta)
        private readonly becaAtletaRepository: Repository<BecasAtleta>,
        @InjectRepository(Beca)
        private readonly becaRepository: Repository<Beca>,
    ) {}

    async create(request: CreateBecasAtletaDto) {
        const becaAtleta = await this.becaAtletaRepository.create(request);
        return await this.becaAtletaRepository.save(becaAtleta);
    }

    async findAll() {
        return await this.becaAtletaRepository.find();
    }

    async findOne(id: string) {
        const subQuery = () => {
            const atleta = this.becaAtletaRepository
                .createQueryBuilder('i')
                .select('i.beca_id')
                .where('atleta_id = :id');
            return atleta.getQuery();
        };
        const beca = await this.becaRepository.createQueryBuilder('j').where('j._id IN (' + subQuery() + ')', { id });
        return await beca.getMany();
    }

    async update(id: string, request: UpdateBecasAtletaDto) {
        const upBecaAtleta = await this.becaAtletaRepository.findOne({ where: { _id: id } });
        upBecaAtleta.atleta = request.atleta;
        upBecaAtleta.beca = request.beca;
        await this.becaAtletaRepository.merge(upBecaAtleta, request);
        return await this.becaAtletaRepository.save(upBecaAtleta);
    }

    async remove(id: string) {
        return await this.becaAtletaRepository.softDelete(id);
    }
}
