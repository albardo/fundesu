import { Test, TestingModule } from '@nestjs/testing';
import { ImgTechadoController } from './img-techado.controller';
import { ImgTechadoService } from './img-techado.service';

describe('ImgTechadoController', () => {
    let controller: ImgTechadoController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgTechadoController],
            providers: [ImgTechadoService],
        }).compile();

        controller = module.get<ImgTechadoController>(ImgTechadoController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
