import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { Estado } from '../estados/entities/estado.entity';
import { Municipio } from '../municipios/entities/municipio.entity';
import { Parroquia } from '../parroquias/entities/parroquia.entity';

@Injectable()
export class LocationsSeeder implements Seeder {
    constructor(
        @InjectRepository(Estado)
        private readonly estadoRepository: Repository<Estado>,
        @InjectRepository(Municipio)
        private readonly municipioRepository: Repository<Municipio>,
        @InjectRepository(Parroquia)
        private readonly parroquiaRepository: Repository<Parroquia>,
    ) {}

    async seed(): Promise<any> {
        const { states } = JSON.parse(readFileSync('./src/modules/seeders/jsons/states.json', 'utf8'));
        const { municipalities } = JSON.parse(readFileSync('./src/modules/seeders/jsons/municipalities.json', 'utf8'));
        const { parishes } = JSON.parse(readFileSync('./src/modules/seeders/jsons/parishes.json', 'utf8'));

        try {
            for await (const state of states) {
                const saveState: Estado = await this.estadoRepository.save(state);

                const municipalitiesFilter = municipalities.filter((x) => x.fields.state_id === saveState.index);

                for await (const municipality of municipalitiesFilter) {
                    const saveMunicipality: Municipio = await this.municipioRepository.save({
                        nombreMunicipio: municipality.fields.name,
                        index: municipality.pk,
                        estado: saveState,
                    });

                    const parishFilter = parishes.filter((x) => x.municipio === saveMunicipality.index);

                    for await (const parish of parishFilter) {
                        await this.parroquiaRepository.save({
                            nombreParroquia: parish.nombreParroquia,
                            municipio: saveMunicipality,
                        });
                    }
                }
            }
        } catch (exception: any) {
            console.log('Crear estado: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const parishes = await this.parroquiaRepository.find();
        await this.parroquiaRepository.remove(parishes);

        const municipalities = await this.municipioRepository.find();
        await this.municipioRepository.remove(municipalities);

        const states = await this.estadoRepository.find();
        return await this.estadoRepository.remove(states);
    }
}
