import { Injectable, NotFoundException } from '@nestjs/common';
import { JWTToken } from './jwt.token';
import { JwtService } from '@nestjs/jwt';
import { UsuariosService } from '../usuarios/usuarios.service';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Injectable()
export class AuthService {
    constructor(private readonly userService: UsuariosService, private readonly jwtService: JwtService) {}

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.userService.emailExists(email);

        if (!user) {
            throw new NotFoundException('User not found');
        }

        const valid = await user.validatePassword(pass);

        if (user && valid) {
            const { contrasena, ...result } = user;
            return result;
        }

        return null;
    }

    async login(user: any): Promise<any> {
        const resp = new JWTToken(this.jwtService, user);
        return resp;
    }

    async profile(user: Usuario): Promise<Usuario | any> {
        const data = await this.userService.findOne(user._id);
        return { data };
    }
}
