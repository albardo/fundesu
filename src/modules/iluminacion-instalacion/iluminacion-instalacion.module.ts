import { Module } from '@nestjs/common';
import { IluminacionInstalacionService } from './iluminacion-instalacion.service';
import { IluminacionInstalacionController } from './iluminacion-instalacion.controller';
import { IluminacionInstalacion } from './entities/iluminacion-instalacion.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([IluminacionInstalacion])],
    controllers: [IluminacionInstalacionController],
    providers: [IluminacionInstalacionService],
})
export class IluminacionInstalacionModule {}
