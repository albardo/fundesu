import { Test, TestingModule } from '@nestjs/testing';
import { ImgTechadoService } from './img-techado.service';

describe('ImgTechadoService', () => {
    let service: ImgTechadoService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgTechadoService],
        }).compile();

        service = module.get<ImgTechadoService>(ImgTechadoService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
