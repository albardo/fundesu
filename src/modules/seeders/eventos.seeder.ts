import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { Evento } from '../eventos/entities/evento.entity';

@Injectable()
export class EventosSeeder implements Seeder {
    constructor(
        @InjectRepository(Evento)
        private readonly eventosRepository: Repository<Evento>,
    ) {}

    async seed(): Promise<any> {
        const { eventos } = JSON.parse(readFileSync('./src/modules/seeders/jsons/eventos.json', 'utf8'));

        try {
            for await (const evento of eventos) {
                await this.eventosRepository.save({ 
                    direccion: evento.direccion,
                    nombreEvento: evento.nombreEvento,
                    fechaEvento: evento.fechaEvento
                });
            }
        } catch (exception: any) {
            console.log('Crear evento deportivo: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const evento = await this.eventosRepository.find();
        return await this.eventosRepository.remove(evento);
    }
}
