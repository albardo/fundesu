import { Module } from '@nestjs/common';
import { ImgParedService } from './img-pared.service';
import { ImgParedController } from './img-pared.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgPared } from './entities/img-pared.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgPared])],
    controllers: [ImgParedController],
    providers: [ImgParedService],
})
export class ImgParedModule {}
