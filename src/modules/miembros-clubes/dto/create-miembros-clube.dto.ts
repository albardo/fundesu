import { IsBoolean, IsDateString, IsUUID } from 'class-validator';
import { Cargo } from 'src/modules/cargos/entities/cargo.entity';
import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { Persona } from 'src/modules/personas/entities/persona.entity';

export class CreateMiembrosClubeDto {
    _id?: string;

    @IsUUID()
    persona: Persona;

    @IsUUID()
    club: Clube;

    @IsUUID()
    cargo: Cargo;

    @IsDateString()
    fechaIngreso: Date;

    @IsDateString()
    fechaSalida?: Date;

    @IsBoolean()
    estatus: boolean;
}
