import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cargo } from '../cargos/entities/cargo.entity';
import { Clube } from '../clubes/entities/clube.entity';
import { CreateMiembrosClubeDto } from './dto/create-miembros-clube.dto';
import { UpdateMiembrosClubeDto } from './dto/update-miembros-clube.dto';
import { MiembrosClube } from './entities/miembros-clube.entity';

@Injectable()
export class MiembrosClubesService {
    constructor(
        @InjectRepository(MiembrosClube)
        private readonly miembroClubRepository: Repository<MiembrosClube>,
    ) {}

    async create(request: CreateMiembrosClubeDto) {
        const miembro = await this.miembroClubRepository.create(request);
        return await this.miembroClubRepository.save(miembro);
    }

    async findAll() {
        return await this.miembroClubRepository.find();
    }

    async findOne(id: string) {
        const complemento = this.miembroClubRepository.createQueryBuilder('i').where('persona_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateMiembrosClubeDto) {
        const upMiembro = await this.miembroClubRepository.findOne({ where: { _id: id } });
        upMiembro.club = request.club;
        upMiembro.persona = request.persona;
        upMiembro.cargo = request.cargo;
        await this.miembroClubRepository.merge(upMiembro, request);
        return await this.miembroClubRepository.save(upMiembro);
    }

    async remove(id: string) {
        return await this.miembroClubRepository.delete(id);
    }
}
