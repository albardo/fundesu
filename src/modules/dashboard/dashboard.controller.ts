import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { DashboardService } from './dashboard.service';
import { CreateDashboardDto } from './dto/create-dashboard.dto';
import { UpdateDashboardDto } from './dto/update-dashboard.dto';

@Controller('dashboard')
export class DashboardController {
    constructor(private readonly dashboardService: DashboardService) {}

    @Post()
    create(@Body() createDashboardDto: CreateDashboardDto) {
        return this.dashboardService.create(createDashboardDto);
    }

    @Get()
    findAll() {
        return this.dashboardService.findAll();
    }

    @AuthAll()
    @Get('all')
    async dashboard() {
        return await this.dashboardService.dashboard();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.dashboardService.findOne(+id);
    }

    @Patch(':id')
    update(@Param('id') id: string, @Body() updateDashboardDto: UpdateDashboardDto) {
        return this.dashboardService.update(+id, updateDashboardDto);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.dashboardService.remove(+id);
    }
}
