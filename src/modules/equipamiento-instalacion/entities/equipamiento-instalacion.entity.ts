import { EquipamientoDepotivoEsp } from 'src/modules/equipamiento-depotivo-esp/entities/equipamiento-depotivo-esp.entity';
import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class EquipamientoInstalacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadEquipamiento: number;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.equipamiento, { eager: true })
    instalaciones: InstalacionesDeportiva;

    @ManyToOne(() => EquipamientoDepotivoEsp, (equipamientoEsp) => equipamientoEsp.equipamiento, { eager: true })
    equipamientoEsp: EquipamientoDepotivoEsp;
}
