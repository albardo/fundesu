import { Test, TestingModule } from '@nestjs/testing';
import { AlturaPesoAtletaController } from './altura-peso-atleta.controller';
import { AlturaPesoAtletaService } from './altura-peso-atleta.service';

describe('AlturaPesoAtletaController', () => {
    let controller: AlturaPesoAtletaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AlturaPesoAtletaController],
            providers: [AlturaPesoAtletaService],
        }).compile();

        controller = module.get<AlturaPesoAtletaController>(AlturaPesoAtletaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
