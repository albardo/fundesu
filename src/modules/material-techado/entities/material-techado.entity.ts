import { Materiale } from '../../materiales/entities/materiale.entity';
import { TechadoInstalacion } from '../../techado-instalacion/entities/techado-instalacion.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

@Entity()
export class MaterialTechado {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadMaterial: number;

    @ManyToOne(() => Materiale, (material) => material.techadoMaterial, { eager: true })
    material: Materiale;

    @ManyToOne(() => TechadoInstalacion, (techado) => techado.techadoMaterial, { eager: true })
    techado: TechadoInstalacion;
}
