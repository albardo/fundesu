import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Beca } from 'src/modules/becas/entities/beca.entity';
import { DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BecasAtleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @ManyToOne(() => Beca, (beca) => beca.becaAtleta, { eager: true })
    beca: Beca;

    @ManyToOne(() => Atleta, (atleta) => atleta.becaAtleta, { eager: true })
    atleta: Atleta;

    @DeleteDateColumn()
    deleted_at?: Date;
}
