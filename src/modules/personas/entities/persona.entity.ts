import { ImgPersona } from 'src/modules/img-persona/entities/img-persona.entity';
import { MiembrosClube } from 'src/modules/miembros-clubes/entities/miembros-clube.entity';
import { Sector } from 'src/modules/sectores/entities/sectore.entity';
import { Usuario } from 'src/modules/usuarios/entities/usuario.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Persona {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar', unique: true })
    cedula: string;

    @Column({ type: 'varchar' })
    nombres: string;

    @Column({ type: 'varchar' })
    apellidos: string;

    @Column({ type: 'varchar' })
    telefono: string;

    @Column({ type: 'varchar' })
    tlfcasa: string;

    @Column({ type: 'varchar' })
    sexo: string;

    @Column()
    fechanac: Date;

    @Column({ type: 'varchar' })
    direccion: string;

    @ManyToOne(() => Sector, (sector) => sector.persona, { eager: true, nullable: true })
    sector: Sector;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => MiembrosClube, (miembroClub) => miembroClub.persona)
    miembroClub: MiembrosClube;

    @OneToMany(() => ImgPersona, (imagePersona) => imagePersona.persona)
    imagePersona: ImgPersona;

    @OneToOne(() => Usuario, (usuario) => usuario.persona)
    usuario: Usuario;
}
