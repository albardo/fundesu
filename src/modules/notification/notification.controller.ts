import { Controller, Delete, Get, Param, Put } from '@nestjs/common';
import { NotificationService } from './notification.service';

@Controller('notificaciones')
export class NotificationController {
    constructor(private readonly notificationService: NotificationService) {}

    @Get()
    findAll() {
        return this.notificationService.findAll();
    }

    @Put()
    updateStatus() {
        return this.notificationService.updateStatusRead();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.notificationService.findOne(id);
    }

    @Delete()
    remove(@Param('id') id: number) {
        return this.notificationService.remove(id);
    }
}
