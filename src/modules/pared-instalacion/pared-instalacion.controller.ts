import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ParedInstalacionService } from './pared-instalacion.service';
import { CreateParedInstalacionDto } from './dto/create-pared-instalacion.dto';
import { UpdateParedInstalacionDto } from './dto/update-pared-instalacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('pared-instalacion')
@ApiTags('Pared-Instalacion')
export class ParedInstalacionController {
    constructor(private readonly paredInstalacionService: ParedInstalacionService) {}

    @Post()
    @ApiBody({ type: [CreateParedInstalacionDto] })
    create(@Res() Res, @Body() createParedInstalacionDto: CreateParedInstalacionDto) {
        this.paredInstalacionService
            .create(createParedInstalacionDto)
            .then((pared) => {
                return Res.status(HttpStatus.CREATED).json(pared);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.paredInstalacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.paredInstalacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateParedInstalacionDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateParedInstalacionDto: UpdateParedInstalacionDto) {
        this.paredInstalacionService
            .update(id, updateParedInstalacionDto)
            .then((pared) => {
                return Res.status(HttpStatus.ACCEPTED).json(pared);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.paredInstalacionService.remove(id);
    }
}
