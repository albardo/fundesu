import { Test, TestingModule } from '@nestjs/testing';
import { MaterialSuperficieController } from './material-superficie.controller';
import { MaterialSuperficieService } from './material-superficie.service';

describe('MaterialSuperficieController', () => {
    let controller: MaterialSuperficieController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MaterialSuperficieController],
            providers: [MaterialSuperficieService],
        }).compile();

        controller = module.get<MaterialSuperficieController>(MaterialSuperficieController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
