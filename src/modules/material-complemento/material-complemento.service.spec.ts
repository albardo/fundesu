import { Test, TestingModule } from '@nestjs/testing';
import { MaterialComplementoService } from './material-complemento.service';

describe('MaterialComplementoService', () => {
    let service: MaterialComplementoService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MaterialComplementoService],
        }).compile();

        service = module.get<MaterialComplementoService>(MaterialComplementoService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
