import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { Discapacidade } from '../discapacidades/entities/discapacidade.entity';

@Injectable()
export class DiscapacidadesSeeder implements Seeder {
    constructor(
        @InjectRepository(Discapacidade)
        private readonly discapacidadesRepository: Repository<Discapacidade>,
    ) {}

    async seed(): Promise<any> {
        const { discapacidades } = JSON.parse(readFileSync('./src/modules/seeders/jsons/discapacidades.json', 'utf8'));

        try {
            for await (const discapacidad of discapacidades) {
                await this.discapacidadesRepository.save({ nombreDiscapacidad: discapacidad });
            }
        } catch (exception: any) {
            console.log('Crear tipo instalacion deportiva: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const discapacidad = await this.discapacidadesRepository.find();
        return await this.discapacidadesRepository.remove(discapacidad);
    }
}
