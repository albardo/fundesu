import { Module } from '@nestjs/common';
import { ImgSuperficieService } from './img-superficie.service';
import { ImgSuperficieController } from './img-superficie.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgSuperficie } from './entities/img-superficie.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgSuperficie])],
    controllers: [ImgSuperficieController],
    providers: [ImgSuperficieService],
})
export class ImgSuperficieModule {}
