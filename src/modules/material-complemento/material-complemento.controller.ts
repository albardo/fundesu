import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { MaterialComplementoService } from './material-complemento.service';
import { CreateMaterialComplementoDto } from './dto/create-material-complemento.dto';
import { UpdateMaterialComplementoDto } from './dto/update-material-complemento.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('material-complemento')
@ApiTags('Material-complemento')
export class MaterialComplementoController {
    constructor(private readonly materialComplementoService: MaterialComplementoService) {}

    @Post()
    @ApiBody({ type: [CreateMaterialComplementoDto] })
    create(@Res() Res, @Body() createMaterialComplementoDto: CreateMaterialComplementoDto) {
        this.materialComplementoService
            .create(createMaterialComplementoDto)
            .then((complemento) => {
                return Res.status(HttpStatus.CREATED).json(complemento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.materialComplementoService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.materialComplementoService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMaterialComplementoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateMaterialComplementoDto: UpdateMaterialComplementoDto) {
        this.materialComplementoService
            .update(id, updateMaterialComplementoDto)
            .then((complemento) => {
                return Res.status(HttpStatus.ACCEPTED).json(complemento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.materialComplementoService.remove(id);
    }
}
