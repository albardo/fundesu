import { Module } from '@nestjs/common';
import { SuperficieInstalacionService } from './superficie-instalacion.service';
import { SuperficieInstalacionController } from './superficie-instalacion.controller';
import { SuperficieInstalacion } from './entities/superficie-instalacion.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([SuperficieInstalacion])],
    controllers: [SuperficieInstalacionController],
    providers: [SuperficieInstalacionService],
})
export class SuperficieInstalacionModule {}
