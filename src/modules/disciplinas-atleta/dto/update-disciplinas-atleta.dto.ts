import { PartialType } from '@nestjs/mapped-types';
import { CreateDisciplinasAtletaDto } from './create-disciplinas-atleta.dto';

export class UpdateDisciplinasAtletaDto extends PartialType(CreateDisciplinasAtletaDto) {}
