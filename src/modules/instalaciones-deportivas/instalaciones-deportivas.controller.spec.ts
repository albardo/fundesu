import { Test, TestingModule } from '@nestjs/testing';
import { InstalacionesDeportivasController } from './instalaciones-deportivas.controller';
import { InstalacionesDeportivasService } from './instalaciones-deportivas.service';

describe('InstalacionesDeportivasController', () => {
    let controller: InstalacionesDeportivasController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [InstalacionesDeportivasController],
            providers: [InstalacionesDeportivasService],
        }).compile();

        controller = module.get<InstalacionesDeportivasController>(InstalacionesDeportivasController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
