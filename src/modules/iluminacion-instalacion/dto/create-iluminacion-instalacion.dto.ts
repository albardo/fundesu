import { IsInt, IsString, IsUUID } from 'class-validator';
import { InstalacionesDeportiva } from '../../instalaciones-deportivas/entities/instalaciones-deportiva.entity';

export class CreateIluminacionInstalacionDto {
    _id?: string;

    @IsString()
    nombreIluminacion: string;

    @IsInt()
    cantidadIluminacion: number;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;
}
