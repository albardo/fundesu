import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { EventosService } from './eventos.service';
import { CreateEventoDto } from './dto/create-evento.dto';
import { UpdateEventoDto } from './dto/update-evento.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';

@Controller('eventos')
@ApiTags('Eventos')
export class EventosController {
    constructor(private readonly eventosService: EventosService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateEventoDto] })
    create(@Request() req, @Res() Res, @Body() createEventoDto: CreateEventoDto) {
        return this.eventosService
            .create(createEventoDto, req.user)
            .then((evento) => {
                return Res.status(HttpStatus.CREATED).json(evento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.eventosService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.eventosService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateEventoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateEventoDto: UpdateEventoDto) {
        return this.eventosService
            .update(id, updateEventoDto)
            .then((evento) => {
                return Res.status(HttpStatus.ACCEPTED).json(evento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.eventosService.remove(id, req.user);
    }
}
