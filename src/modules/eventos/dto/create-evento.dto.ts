import { IsDateString, IsString } from 'class-validator';

export class CreateEventoDto {
    _id?: string;

    @IsString()
    nombreEvento: string;

    @IsDateString()
    fechaEvento: Date;

    @IsString()
    direccion: string;
}
