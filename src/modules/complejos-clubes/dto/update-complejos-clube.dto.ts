import { PartialType } from '@nestjs/mapped-types';
import { CreateComplejosClubeDto } from './create-complejos-clube.dto';

export class UpdateComplejosClubeDto extends PartialType(CreateComplejosClubeDto) {}
