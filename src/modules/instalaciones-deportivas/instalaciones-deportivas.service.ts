import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateInstalacionesDeportivaDto } from './dto/create-instalaciones-deportiva.dto';
import { UpdateInstalacionesDeportivaDto } from './dto/update-instalaciones-deportiva.dto';
import { InstalacionesDeportiva } from './entities/instalaciones-deportiva.entity';
import { ViewInstalacion } from './entities/viewInstalaciones.entity';

@Injectable()
export class InstalacionesDeportivasService {
    constructor(
        @InjectRepository(InstalacionesDeportiva)
        private readonly instalacionRepository: Repository<InstalacionesDeportiva>,
        @InjectRepository(ViewInstalacion)
        private readonly viewInstalacionRepository: Repository<ViewInstalacion>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
    ) {}

    async create(request: CreateInstalacionesDeportivaDto, user: any) {
        let instalacion = this.instalacionRepository.create(request);
        instalacion = await this.instalacionRepository.save(instalacion);

        const message = 'Una instalación deportiva a sido registrada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: InstalacionesDeportiva.name,
            uuid: instalacion._id,
            data: instalacion,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return instalacion;
    }

    async findAll() {
        return await this.instalacionRepository.find();
    }

    async findOne(id: string) {
        return await this.instalacionRepository.findOne({ where: { _id: id } });
    }

    async count() {
        const count = await await this.instalacionRepository.count();
        return count;
    }

    async recuperacionCount() {
        const statusTrue = await this.instalacionRepository.count({
            where: { recuperacionInstalacion: true },
        });

        const statusFalse = await this.instalacionRepository.count({
            where: { recuperacionInstalacion: false },
        });

        return { statusTrue, statusFalse };
    }

    async statusCount() {
        const aptas = await this.instalacionRepository.count({
            where: { estadoInstalacion: 'Apta para competencia' },
        });

        const optima = await this.instalacionRepository.count({
            where: { estadoInstalacion: 'Optimas condiciones' },
        });

        const deteriorada = await this.instalacionRepository.count({
            where: { estadoInstalacion: 'Deteriorada' },
        });

        const malEstado = await this.instalacionRepository.count({
            where: { estadoInstalacion: 'Muy mal estado' },
        });

        return { aptas, optima, deteriorada, malEstado };
    }

    async viewInstlaciones(id: string) {
        return await this.viewInstalacionRepository.find({ where: { _id: id } });
    }

    async update(id: string, request: UpdateInstalacionesDeportivaDto) {
        const upInstalacion = await this.instalacionRepository.findOne({ where: { _id: id } });
        upInstalacion.complejosDeportivos = request.complejosDeportivos;
        upInstalacion.tipoInstalacionDep = request.tipoInstalacionDep;
        await this.instalacionRepository.merge(upInstalacion, request);
        return await this.instalacionRepository.save(upInstalacion);
    }

    async remove(id: string, user: any) {
        const instalacion = await this.instalacionRepository.findOne({ where: { _id: id } });

        const message = 'Una instalación deportiva a sido eliminada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: InstalacionesDeportiva.name,
            uuid: id,
            data: instalacion,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.instalacionRepository.softDelete(id);
    }
}
