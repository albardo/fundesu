import { Module } from '@nestjs/common';
import { EventosAtletasService } from './eventos-atletas.service';
import { EventosAtletasController } from './eventos-atletas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventosAtleta } from './entities/eventos-atleta.entity';
import { Evento } from '../eventos/entities/evento.entity';

@Module({
    imports: [TypeOrmModule.forFeature([EventosAtleta, Evento])],
    controllers: [EventosAtletasController],
    providers: [EventosAtletasService],
})
export class EventosAtletasModule {}
