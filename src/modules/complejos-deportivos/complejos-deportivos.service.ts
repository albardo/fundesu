import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateComplejosDeportivoDto } from './dto/create-complejos-deportivo.dto';
import { UpdateComplejosDeportivoDto } from './dto/update-complejos-deportivo.dto';
import { ComplejosDeportivo } from './entities/complejos-deportivo.entity';

@Injectable()
export class ComplejosDeportivosService {
    constructor(
        @InjectRepository(ComplejosDeportivo)
        private readonly complejosDeportivosRepository: Repository<ComplejosDeportivo>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
    ) {}

    async create(request: CreateComplejosDeportivoDto, user: any): Promise<ComplejosDeportivo> {
        let complejosD = this.complejosDeportivosRepository.create(request);
        complejosD = await this.complejosDeportivosRepository.save(complejosD);

        const message = 'Un complejo deportivo a sido registrado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: ComplejosDeportivo.name,
            uuid: complejosD._id,
            data: complejosD,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return complejosD;
    }

    async findAll() {
        return await this.complejosDeportivosRepository.find();
    }

    async findOne(id: string) {
        return await this.complejosDeportivosRepository.findOne({ where: { _id: id } });
    }

    async count() {
        const count = await this.complejosDeportivosRepository.count();
        return count;
    }

    async update(id: string, request: UpdateComplejosDeportivoDto): Promise<ComplejosDeportivo> {
        const upComplejo = await this.complejosDeportivosRepository.findOne({ where: { _id: id } });
        upComplejo.sector = request.sector;
        this.complejosDeportivosRepository.merge(upComplejo, request);
        return await this.complejosDeportivosRepository.save(upComplejo);
    }

    async remove(id: string, user: any) {
        const complejo = await this.complejosDeportivosRepository.findOne({ where: { _id: id } });

        const message = 'Un complejo deportivo a sido eliminado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: ComplejosDeportivo.name,
            uuid: id,
            data: complejo,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.complejosDeportivosRepository.softDelete(id);
    }
}
