import { Module } from '@nestjs/common';
import { ComplejosDeportivosService } from './complejos-deportivos.service';
import { ComplejosDeportivosController } from './complejos-deportivos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComplejosDeportivo } from './entities/complejos-deportivo.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ComplejosDeportivo, Notification, Usuario])],
    controllers: [ComplejosDeportivosController],
    providers: [ComplejosDeportivosService],
})
export class ComplejosDeportivosModule {}
