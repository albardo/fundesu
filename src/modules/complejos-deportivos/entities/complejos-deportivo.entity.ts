import { ComplejosClube } from 'src/modules/complejos-clubes/entities/complejos-clube.entity';
import { ImgComplejoDep } from 'src/modules/img-complejo-dep/entities/img-complejo-dep.entity';
import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Sector } from 'src/modules/sectores/entities/sectore.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ComplejosDeportivo {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreComplejoDeportivo: string;

    @Column({ type: 'varchar' })
    direccion: string;

    @ManyToOne(() => Sector, (sector) => sector.complejoD, { eager: true })
    sector: Sector;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => InstalacionesDeportiva, (instalacionesDeportiva) => instalacionesDeportiva.complejosDeportivos)
    instalacionDeportiva: InstalacionesDeportiva;

    @OneToMany(() => ComplejosClube, (complejoClub) => complejoClub.complejo)
    complejoClub: ComplejosClube;

    @OneToMany(() => ImgComplejoDep, (imageComplejo) => imageComplejo.complejo)
    imageComplejo: ImgComplejoDep;
}
