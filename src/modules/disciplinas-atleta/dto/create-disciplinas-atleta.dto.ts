import { IsBoolean, IsString, IsUUID } from 'class-validator';
import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { DisciplinasDeportiva } from 'src/modules/disciplinas-deportivas/entities/disciplinas-deportiva.entity';

export class CreateDisciplinasAtletaDto {
    _id?: string;

    @IsString()
    categoria: string;

    @IsString()
    division: string;

    @IsBoolean()
    estatus: boolean;

    @IsUUID()
    disciplina: DisciplinasDeportiva;

    @IsUUID()
    atleta: Atleta;
}
