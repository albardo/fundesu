import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Atleta } from '../atletas/entities/atleta.entity';
import { InstalacionesDeportiva } from '../instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { CreateDashboardDto } from './dto/create-dashboard.dto';
import { UpdateDashboardDto } from './dto/update-dashboard.dto';

@Injectable()
export class DashboardService {
    constructor(
        @InjectRepository(InstalacionesDeportiva)
        private readonly instalacionesRepository: Repository<InstalacionesDeportiva>,
        @InjectRepository(Atleta)
        private readonly atletaRepositoryRepostory: Repository<Atleta>,
    ) {}

    create(createDashboardDto: CreateDashboardDto) {
        return 'This action adds a new dashboard';
    }

    findAll() {
        return `This action returns all dashboard`;
    }

    findOne(id: number) {
        return `This action returns a #${id} dashboard`;
    }

    update(id: number, updateDashboardDto: UpdateDashboardDto) {
        return `This action updates a #${id} dashboard`;
    }

    remove(id: number) {
        return `This action removes a #${id} dashboard`;
    }

    async dashboard() {
        const totalInstalaciones = await this.instalacionesRepository.count();

        const totalAtletas = await this.atletaRepositoryRepostory.count();

        let instalacionRecuperada: any = this.instalacionesRepository.createQueryBuilder('i').where('1 = 1');
        instalacionRecuperada.andWhere('i.recuperacionInstalacion = :recuperada', { recuperada: true });
        instalacionRecuperada = await instalacionRecuperada.getCount();

        const indicadorInstalacionRecuperada = this.calularPorcentajeAlto(
            totalInstalaciones,
            instalacionRecuperada,
            70,
            30,
        );

        let instalacionMalEstado: any = this.instalacionesRepository.createQueryBuilder('i').where('1 = 1');
        instalacionMalEstado.andWhere('i.estadoInstalacion = :estado', { estado: 'Deteriorada' });
        instalacionMalEstado.andWhere('i.estadoInstalacion = :estado', { estado: 'Muy mal estado' });
        instalacionMalEstado = await instalacionMalEstado.getCount();

        const indicadorInstalacionMalEstado = this.calularPorcentajeBajo(
            totalInstalaciones,
            instalacionMalEstado,
            20,
            75,
        );

        let instalacionRecuperacion: any = this.instalacionesRepository.createQueryBuilder('i').where('1 = 1');
        instalacionRecuperacion.andWhere('i.estadoInstalacion = :estado', { estado: 'En recuperacion' });
        instalacionRecuperacion = await instalacionRecuperacion.getCount();

        const indicadorInstalacionRecuperacion = this.calularPorcentajeAlto(
            totalInstalaciones,
            instalacionMalEstado,
            75,
            20,
        );

        let atletaActivo: any = this.atletaRepositoryRepostory.createQueryBuilder('i').where('1 = 1');
        atletaActivo.innerJoinAndSelect('i.disciplinadAtleta', 'disciplinadAtleta');
        atletaActivo.andWhere('disciplinadAtleta.estatus = :estatus', { estatus: true });
        atletaActivo = await atletaActivo.getCount();

        const indicadorAtletaActivo = this.calularPorcentajeAlto(totalAtletas, atletaActivo, 75, 25);

        let atletaInactivo: any = this.atletaRepositoryRepostory.createQueryBuilder('i').where('1 = 1');
        atletaInactivo.innerJoinAndSelect('i.disciplinadAtleta', 'disciplinadAtleta');
        atletaInactivo.andWhere('disciplinadAtleta.estatus = :estatus', { estatus: false });
        atletaInactivo = await atletaInactivo.getCount();

        const indicadorAtletaInactivo = this.calularPorcentajeBajo(totalAtletas, atletaInactivo, 20, 80);

        let atletaWithInstalacion: any = this.atletaRepositoryRepostory.createQueryBuilder('i').where('1 = 1');
        atletaWithInstalacion.innerJoinAndSelect('i.atletaClub', 'atletaClub');
        atletaWithInstalacion.innerJoinAndSelect('atletaClub.club', 'club');
        atletaWithInstalacion.innerJoinAndSelect('club.complejoClub', 'complejoClub');
        atletaWithInstalacion.innerJoinAndSelect('complejoClub.complejo', 'complejo');
        atletaWithInstalacion.innerJoinAndSelect('complejo.instalacionDeportiva', 'instalacionDeportiva');
        atletaWithInstalacion = await atletaWithInstalacion.getCount();

        const indicadorAtletaWithInstalacion = this.calularPorcentajeAlto(totalAtletas, atletaInactivo, 80, 20);

        let atletaWithoutInstalacion: any = this.atletaRepositoryRepostory.createQueryBuilder('i').where('1 = 1');
        atletaWithoutInstalacion = await atletaWithoutInstalacion.getCount();

        atletaWithoutInstalacion = atletaWithoutInstalacion - atletaWithInstalacion;

        const indicadorAtletaWithoutInstalacion = this.calularPorcentajeBajo(totalAtletas, atletaInactivo, 20, 80);

        const data = {
            instalacionRecuperada: {
                totalInstalaciones: totalInstalaciones,
                instalacionRecuperada: instalacionRecuperada,
                indicador: indicadorInstalacionRecuperada.indicador,
                porcentaje: indicadorInstalacionRecuperada.porcentaje,
            },
            instalacionMalEstado: {
                totalInstalaciones: totalInstalaciones,
                instalacionMalEstado: instalacionMalEstado,
                indicador: indicadorInstalacionMalEstado.indicador,
                porcentaje: indicadorInstalacionMalEstado.porcentaje,
            },
            instalacionRecuperacion: {
                totalInstalaciones: totalInstalaciones,
                instalacionRecuperacion: instalacionRecuperacion,
                indicador: indicadorInstalacionRecuperacion.indicador,
                porcentaje: indicadorInstalacionRecuperacion.porcentaje,
            },
            atletaActivo: {
                totalAtletas: totalAtletas,
                atletaActivo: atletaActivo,
                indicador: indicadorAtletaActivo.indicador,
                porcentaje: indicadorAtletaActivo.porcentaje,
            },
            atletaInactivo: {
                totalAtletas: totalAtletas,
                atletaInactivo: atletaInactivo,
                indicador: indicadorAtletaInactivo.indicador,
                porcentaje: indicadorAtletaInactivo.porcentaje,
            },
            atletaWithInstalacion: {
                totalAtletas: totalAtletas,
                atletaWithInstalacion: atletaWithInstalacion,
                indicador: indicadorAtletaWithInstalacion.indicador,
                porcentaje: indicadorAtletaWithInstalacion.porcentaje,
            },
            atletaWithoutInstalacion: {
                totalAtletas: totalAtletas,
                atletaWithoutInstalacion: atletaWithoutInstalacion,
                indicador: indicadorAtletaWithoutInstalacion.indicador,
                porcentaje: indicadorAtletaWithoutInstalacion.porcentaje,
            },
        };

        return data;
    }

    calularPorcentajeAlto(total: number, cantidad: number, indicadorExcelente: number, indicadorMalo: number) {
        const porcentaje = (total * cantidad) / 100;

        let indicador = '';

        if (porcentaje >= indicadorExcelente) {
            indicador = 'Excelente';
        } else if (porcentaje > indicadorExcelente && porcentaje < indicadorMalo) {
            indicador = 'Bueno';
        } else {
            indicador = 'Critico';
        }

        const resultado = {
            porcentaje,
            indicador,
        };

        return resultado;
    }

    calularPorcentajeBajo(total: number, cantidad: number, indicadorExcelente: number, indicadorMalo: number) {
        const porcentaje = (total * cantidad) / 100;

        let indicador = '';

        if (porcentaje <= indicadorExcelente) {
            indicador = 'Excelente';
        } else if (porcentaje > indicadorExcelente && porcentaje < indicadorMalo) {
            indicador = 'Bueno';
        } else {
            indicador = 'Critico';
        }

        const resultado = {
            porcentaje,
            indicador,
        };

        return resultado;
    }
}
