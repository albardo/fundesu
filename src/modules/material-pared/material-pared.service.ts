import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Materiale } from '../materiales/entities/materiale.entity';
import { CreateMaterialParedDto } from './dto/create-material-pared.dto';
import { UpdateMaterialParedDto } from './dto/update-material-pared.dto';
import { MaterialPared } from './entities/material-pared.entity';

@Injectable()
export class MaterialParedService {
    constructor(
        @InjectRepository(MaterialPared)
        private readonly materialParedRepository: Repository<MaterialPared>,
        @InjectRepository(Materiale)
        private readonly materialRespository: Repository<Materiale>,
    ) {}

    async create(request: CreateMaterialParedDto) {
        const pared = await this.materialParedRepository.create(request);
        return await this.materialParedRepository.save(pared);
    }

    async findAll() {
        return await this.materialParedRepository.find();
    }

    async findOne(id: string) {
        const material = await this.materialParedRepository.find({
            relations: [],
            where: {
                pared: {
                    _id: id,
                },
            },
        });
        return material;
    }

    async update(id: string, request: UpdateMaterialParedDto) {
        const upPared = await this.materialParedRepository.findOne({ where: { _id: id } });
        upPared.pared = request.pared;
        upPared.material = request.material;
        await this.materialParedRepository.merge(upPared, request);
        return await this.materialParedRepository.save(upPared);
    }

    async remove(id: string) {
        return await this.materialParedRepository.delete(id);
    }
}
