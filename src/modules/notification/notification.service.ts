import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { Notification } from './entities/notification.entity';

@Injectable()
export class NotificationService {
    constructor(
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    create(createNotificationDto: CreateNotificationDto) {
        return 'This action adds a new notification';
    }

    async findAll() {
        return await this.notificationRepository.find({ order: { createdAt: 'DESC' } });
    }

    async findOne(id: string) {
        return await this.notificationRepository.findOne({ where: { _id: id } });
    }

    async updateStatusRead() {
        const updateStatus = await this.notificationRepository.query(
            'UPDATE notification SET read = true WHERE 1 = 1 RETURNING notification.*;',
        );

        const update = updateStatus[0];

        return update;
    }

    update(id: string, updateNotificationDto: UpdateNotificationDto) {
        return `This action updates a #${id} notification`;
    }

    remove(id: number) {
        return `This action removes a #${id} notification`;
    }

    async saveNotification(message: string, userId: any, entity: string, dataEntity: object) {
        const usuario = await this.usuarioRepository.findOne({ where: { _id: userId } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: entity,
            uuid: dataEntity['_id'],
            data: dataEntity,
            user: usuario,
        });

        await this.notificationRepository.save(notification);
    }
}
