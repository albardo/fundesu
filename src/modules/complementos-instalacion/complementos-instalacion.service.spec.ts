import { Test, TestingModule } from '@nestjs/testing';
import { ComplementosInstalacionService } from './complementos-instalacion.service';

describe('ComplementosInstalacionService', () => {
    let service: ComplementosInstalacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ComplementosInstalacionService],
        }).compile();

        service = module.get<ComplementosInstalacionService>(ComplementosInstalacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
