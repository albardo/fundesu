import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgParedDto } from './dto/create-img-pared.dto';
import { UpdateImgParedDto } from './dto/update-img-pared.dto';
import { ImgPared } from './entities/img-pared.entity';

@Injectable()
export class ImgParedService {
    constructor(
        @InjectRepository(ImgPared)
        private readonly imgParedRepository: Repository<ImgPared>,
    ) {}

    async create(request: CreateImgParedDto) {
        const img = await this.imgParedRepository.create(request);
        return await this.imgParedRepository.save(img);
    }

    async findAll() {
        return await this.imgParedRepository.find();
    }

    async findOne(id: string) {
        return await this.imgParedRepository.findOne({
            where: {
                instalaciones: { _id: id },
            },
        });
    }

    async update(id: string, request: UpdateImgParedDto) {
        const upImg = await this.imgParedRepository.findOne({ where: { _id: id } });
        upImg.instalaciones = request.instalaciones;
        await this.imgParedRepository.merge(upImg, request);
        return await this.imgParedRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgParedRepository.delete(id);
    }
}
