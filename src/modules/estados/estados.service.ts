import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEstadoDto } from './dto/create-estado.dto';
import { UpdateEstadoDto } from './dto/update-estado.dto';
import { Estado } from './entities/estado.entity';

@Injectable()
export class EstadosService {
    constructor(
        @InjectRepository(Estado)
        private readonly estadorepository: Repository<Estado>,
    ) {}

    create(request: CreateEstadoDto) {
        const state = this.estadorepository.create(request);
        return this.estadorepository.save(state);
    }

    findAll(): Promise<Estado[]> {
        return this.estadorepository.find();
    }

    findOne(_id: string) {
        return this.estadorepository.findOne({ where: { _id } });
    }

    async update(id: string, request: UpdateEstadoDto) {
        const upEstado = await this.estadorepository.findOne({ where: { _id: id } });
        await this.estadorepository.merge(upEstado, request);
        return await this.estadorepository.save(upEstado);
    }

    async remove(id: string) {
        return await this.estadorepository.delete(id);
    }
}
