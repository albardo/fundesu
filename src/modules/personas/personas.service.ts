import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePersonaDto } from './dto/create-persona.dto';
import { UpdatePersonaDto } from './dto/update-persona.dto';
import { Persona } from './entities/persona.entity';

@Injectable()
export class PersonasService {
    constructor(
        @InjectRepository(Persona)
        private readonly personaRepository: Repository<Persona>,
    ) {}

    async create(request: CreatePersonaDto): Promise<Persona> {
        const person = await this.personaRepository.create(request);
        return this.personaRepository.save(person);
    }

    async findAll() {
        return await this.personaRepository.find();
    }

    async findOne(_id: string): Promise<Persona> {
        return await this.personaRepository.findOne({ where: { _id } });
    }

    async update(_id: string, request: UpdatePersonaDto): Promise<Persona> {
        const upPerson = await this.personaRepository.findOne({ where: { _id } });
        upPerson.sector = request.sector;
        this.personaRepository.merge(upPerson, request);
        return this.personaRepository.save(upPerson);
    }

    async remove(_id: string): Promise<any> {
        return await this.personaRepository.softDelete(_id);
    }
}
