import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { AtletasService } from './atletas.service';
import { CreateAtletaDto } from './dto/create-atleta.dto';
import { UpdateAtletaDto } from './dto/update-atleta.dto';

@Controller('atletas')
@ApiTags('Atletas')
export class AtletasController {
    constructor(private readonly atletasService: AtletasService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateAtletaDto] })
    create(@Request() req, @Res() Res, @Body() createAtletaDto: CreateAtletaDto) {
        this.atletasService
            .create(createAtletaDto, req.user)
            .then((atleta) => {
                return Res.status(HttpStatus.CREATED).json(atleta);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.atletasService.findAll();
    }

    @Get('/view/:id')
    view(@Param('id') id: string) {
        return this.atletasService.view(id);
    }

    @Get('/total-atletas')
    countAll() {
        return this.atletasService.count();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.atletasService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateAtletaDto] })
    async update(@Res() Res, @Param('id') id: string, @Body() updateAtletaDto: UpdateAtletaDto) {
        return this.atletasService
            .update(id, updateAtletaDto)
            .then((atleta) => {
                return Res.status(HttpStatus.ACCEPTED).json(atleta);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.atletasService.remove(id, req.user);
    }
}
