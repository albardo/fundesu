import { PartialType } from '@nestjs/mapped-types';
import { CreateDiscapacidadeDto } from './create-discapacidade.dto';

export class UpdateDiscapacidadeDto extends PartialType(CreateDiscapacidadeDto) {}
