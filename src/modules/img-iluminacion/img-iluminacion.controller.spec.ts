import { Test, TestingModule } from '@nestjs/testing';
import { ImgIluminacionController } from './img-iluminacion.controller';
import { ImgIluminacionService } from './img-iluminacion.service';

describe('ImgIluminacionController', () => {
    let controller: ImgIluminacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgIluminacionController],
            providers: [ImgIluminacionService],
        }).compile();

        controller = module.get<ImgIluminacionController>(ImgIluminacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
