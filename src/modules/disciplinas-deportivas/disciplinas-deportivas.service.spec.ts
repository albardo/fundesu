import { Test, TestingModule } from '@nestjs/testing';
import { DisciplinasDeportivasService } from './disciplinas-deportivas.service';

describe('DisciplinasDeportivasService', () => {
    let service: DisciplinasDeportivasService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DisciplinasDeportivasService],
        }).compile();

        service = module.get<DisciplinasDeportivasService>(DisciplinasDeportivasService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
