import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateIluminacionInstalacionDto } from './dto/create-iluminacion-instalacion.dto';
import { UpdateIluminacionInstalacionDto } from './dto/update-iluminacion-instalacion.dto';
import { IluminacionInstalacion } from './entities/iluminacion-instalacion.entity';

@Injectable()
export class IluminacionInstalacionService {
    constructor(
        @InjectRepository(IluminacionInstalacion)
        private readonly iluminacionInstRespository: Repository<IluminacionInstalacion>,
    ) {}

    async create(request: CreateIluminacionInstalacionDto) {
        const iluminacion = await this.iluminacionInstRespository.create(request);
        return await this.iluminacionInstRespository.save(iluminacion);
    }

    async findAll() {
        return await this.iluminacionInstRespository.find();
    }

    async findOne(id: string) {
        const complemento = await this.iluminacionInstRespository
            .createQueryBuilder('i')
            .where('instalaciones_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateIluminacionInstalacionDto) {
        const upIluminacion = await this.iluminacionInstRespository.findOne({ where: { _id: id } });
        upIluminacion.instalaciones = request.instalaciones;
        await this.iluminacionInstRespository.merge(upIluminacion, request);
        return await this.iluminacionInstRespository.save(upIluminacion);
    }

    async remove(id: string) {
        return await this.iluminacionInstRespository.delete(id);
    }
}
