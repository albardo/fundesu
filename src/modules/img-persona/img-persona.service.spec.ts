import { Test, TestingModule } from '@nestjs/testing';
import { ImgPersonaService } from './img-persona.service';

describe('ImgPersonaService', () => {
    let service: ImgPersonaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgPersonaService],
        }).compile();

        service = module.get<ImgPersonaService>(ImgPersonaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
