import { PartialType } from '@nestjs/mapped-types';
import { CreateTipoInstalacionDepDto } from './create-tipo-instalacion-dep.dto';

export class UpdateTipoInstalacionDepDto extends PartialType(CreateTipoInstalacionDepDto) {}
