import { Test, TestingModule } from '@nestjs/testing';
import { ImplementosDeportivosService } from './implementos-deportivos.service';

describe('ImplementosDeportivosService', () => {
    let service: ImplementosDeportivosService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImplementosDeportivosService],
        }).compile();

        service = module.get<ImplementosDeportivosService>(ImplementosDeportivosService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
