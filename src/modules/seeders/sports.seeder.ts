import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { DisciplinasDeportiva } from '../disciplinas-deportivas/entities/disciplinas-deportiva.entity';

@Injectable()
export class SportsSeeder implements Seeder {
    constructor(
        @InjectRepository(DisciplinasDeportiva)
        private readonly disciplinasDeportivaRepository: Repository<DisciplinasDeportiva>,
    ) {}

    async seed(): Promise<any> {
        const { sports } = JSON.parse(readFileSync('./src/modules/seeders/jsons/sports.json', 'utf8'));

        try {
            for await (const sportName of sports) {
                await this.disciplinasDeportivaRepository.save({ nombreDisciplina: sportName });
            }
        } catch (exception: any) {
            console.log('Crear disciplina deportiva: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const sports = await this.disciplinasDeportivaRepository.find();
        return await this.disciplinasDeportivaRepository.remove(sports);
    }
}
