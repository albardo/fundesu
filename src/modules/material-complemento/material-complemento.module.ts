import { Module } from '@nestjs/common';
import { MaterialComplementoService } from './material-complemento.service';
import { MaterialComplementoController } from './material-complemento.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MaterialComplemento } from './entities/material-complemento.entity';
import { Materiale } from '../materiales/entities/materiale.entity';

@Module({
    imports: [TypeOrmModule.forFeature([MaterialComplemento, Materiale])],
    controllers: [MaterialComplementoController],
    providers: [MaterialComplementoService],
})
export class MaterialComplementoModule {}
