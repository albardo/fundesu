import { Test, TestingModule } from '@nestjs/testing';
import { TipoAtletaController } from './tipo-atleta.controller';
import { TipoAtletaService } from './tipo-atleta.service';

describe('TipoAtletaController', () => {
    let controller: TipoAtletaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [TipoAtletaController],
            providers: [TipoAtletaService],
        }).compile();

        controller = module.get<TipoAtletaController>(TipoAtletaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
