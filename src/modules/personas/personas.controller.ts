import { Controller, Get, Post, Body, Param, Delete, Res, HttpStatus, Put } from '@nestjs/common';
import { PersonasService } from './personas.service';
import { CreatePersonaDto } from './dto/create-persona.dto';
import { UpdatePersonaDto } from './dto/update-persona.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('personas')
@ApiTags('Personas')
export class PersonasController {
    constructor(private readonly personasService: PersonasService) {}

    @Post()
    @ApiBody({ type: [CreatePersonaDto] })
    create(@Res() Res, @Body() createPersonaDto: CreatePersonaDto) {
        return this.personasService
            .create(createPersonaDto)
            .then((persona) => {
                return Res.status(HttpStatus.CREATED).json(persona);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Get()
    findAll() {
        return this.personasService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') _id: string) {
        return this.personasService.findOne(_id);
    }

    @Put(':id')
    @ApiBody({ type: [CreatePersonaDto] })
    update(@Res() Res, @Param('id') _id: string, @Body() updatePersonaDto: UpdatePersonaDto) {
        return this.personasService
            .update(_id, updatePersonaDto)
            .then((persona) => {
                return Res.status(HttpStatus.CREATED).json(persona);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Delete(':id')
    remove(@Param('id') _id: string) {
        return this.personasService.remove(_id);
    }
}
