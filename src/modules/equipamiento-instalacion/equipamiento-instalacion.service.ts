import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEquipamientoInstalacionDto } from './dto/create-equipamiento-instalacion.dto';
import { UpdateEquipamientoInstalacionDto } from './dto/update-equipamiento-instalacion.dto';
import { EquipamientoInstalacion } from './entities/equipamiento-instalacion.entity';

@Injectable()
export class EquipamientoInstalacionService {
    constructor(
        @InjectRepository(EquipamientoInstalacion)
        private readonly equipamientoDeporRepository: Repository<EquipamientoInstalacion>,
    ) {}

    async create(request: CreateEquipamientoInstalacionDto) {
        const equipamiento = this.equipamientoDeporRepository.create(request);
        return await this.equipamientoDeporRepository.save(equipamiento);
    }

    async findAll() {
        return await this.equipamientoDeporRepository.find();
    }

    async findOne(id: string) {
        const complemento = await this.equipamientoDeporRepository
            .createQueryBuilder('i')
            .where('instalaciones_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateEquipamientoInstalacionDto) {
        const upEquipamiento = await this.equipamientoDeporRepository.findOne({ where: { _id: id } });
        upEquipamiento.equipamientoEsp = request.equipamientoEsp;
        upEquipamiento.instalaciones = request.instalaciones;
        this.equipamientoDeporRepository.merge(upEquipamiento, request);
        return await this.equipamientoDeporRepository.save(upEquipamiento);
    }

    async remove(id: string) {
        return await this.equipamientoDeporRepository.delete(id);
    }
}
