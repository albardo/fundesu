import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class AtletasClub {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @ManyToOne(() => Atleta, (atleta) => atleta.atletaClub, { eager: true })
    atleta: Atleta;

    @ManyToOne(() => Clube, (club) => club.atletaClub, { eager: true })
    club: Clube;

    @Column({ type: 'date' })
    fechaIngreso: Date;

    @Column({ type: 'date', nullable: true })
    fechaSalida: Date;

    @Column({ type: 'boolean' })
    estatus: boolean;
}
