import { PartialType } from '@nestjs/mapped-types';
import { CreateInstalacionesDeportivaDto } from './create-instalaciones-deportiva.dto';

export class UpdateInstalacionesDeportivaDto extends PartialType(CreateInstalacionesDeportivaDto) {}
