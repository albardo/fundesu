import { PartialType } from '@nestjs/mapped-types';
import { CreateMaterialSuperficieDto } from './create-material-superficie.dto';

export class UpdateMaterialSuperficieDto extends PartialType(CreateMaterialSuperficieDto) {}
