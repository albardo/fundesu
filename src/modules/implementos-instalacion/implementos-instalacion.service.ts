import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImplementosInstalacionDto } from './dto/create-implementos-instalacion.dto';
import { UpdateImplementosInstalacionDto } from './dto/update-implementos-instalacion.dto';
import { ImplementosInstalacion } from './entities/implementos-instalacion.entity';

@Injectable()
export class ImplementosInstalacionService {
    constructor(
        @InjectRepository(ImplementosInstalacion)
        private readonly implementoInstRepository: Repository<ImplementosInstalacion>,
    ) {}

    async create(request: CreateImplementosInstalacionDto) {
        const implemento = await this.implementoInstRepository.create(request);
        return await this.implementoInstRepository.create(implemento);
    }

    async findAll() {
        return await this.implementoInstRepository.find();
    }

    async findOne(id: string) {
        const complemento = await this.implementoInstRepository
            .createQueryBuilder('i')
            .where('instalaciones_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateImplementosInstalacionDto) {
        const upImplemento = await this.implementoInstRepository.findOne({ where: { _id: id } });
        upImplemento.implemento = request.implemento;
        upImplemento.instalaciones = request.instalaciones;
        await this.implementoInstRepository.merge(upImplemento, request);
        return await this.implementoInstRepository.save(upImplemento);
    }

    async remove(id: string) {
        return await this.implementoInstRepository.delete(id);
    }
}
