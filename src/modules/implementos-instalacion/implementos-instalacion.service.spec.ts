import { Test, TestingModule } from '@nestjs/testing';
import { ImplementosInstalacionService } from './implementos-instalacion.service';

describe('ImplementosInstalacionService', () => {
    let service: ImplementosInstalacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImplementosInstalacionService],
        }).compile();

        service = module.get<ImplementosInstalacionService>(ImplementosInstalacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
