import { Test, TestingModule } from '@nestjs/testing';
import { InstalacionesDeportivasService } from './instalaciones-deportivas.service';

describe('InstalacionesDeportivasService', () => {
    let service: InstalacionesDeportivasService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [InstalacionesDeportivasService],
        }).compile();

        service = module.get<InstalacionesDeportivasService>(InstalacionesDeportivasService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
