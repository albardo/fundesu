import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res, Request } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { DisciplinasDeportivasService } from './disciplinas-deportivas.service';
import { CreateDisciplinasDeportivaDto } from './dto/create-disciplinas-deportiva.dto';
import { UpdateDisciplinasDeportivaDto } from './dto/update-disciplinas-deportiva.dto';

@Controller('disciplinas-deportivas')
@ApiTags('Disciplinas-deportivas')
export class DisciplinasDeportivasController {
    constructor(private readonly disciplinasDeportivasService: DisciplinasDeportivasService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateDisciplinasDeportivaDto] })
    create(@Request() req, @Res() Res, @Body() createDisciplinasDeportivaDto: CreateDisciplinasDeportivaDto) {
        this.disciplinasDeportivasService
            .create(createDisciplinasDeportivaDto, req.user)
            .then((disciplinas) => {
                return Res.status(HttpStatus.CREATED).json(disciplinas);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Get()
    findAll() {
        return this.disciplinasDeportivasService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.disciplinasDeportivasService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateDisciplinasDeportivaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateDisciplinasDeportivaDto: UpdateDisciplinasDeportivaDto) {
        this.disciplinasDeportivasService
            .update(id, updateDisciplinasDeportivaDto)
            .then((disciplinas) => {
                return Res.status(HttpStatus.ACCEPTED).json(disciplinas);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.disciplinasDeportivasService.remove(id, req.user);
    }
}
