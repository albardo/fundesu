import { PartialType } from '@nestjs/mapped-types';
import { CreateTallaAtletaDto } from './create-talla-atleta.dto';

export class UpdateTallaAtletaDto extends PartialType(CreateTallaAtletaDto) {}
