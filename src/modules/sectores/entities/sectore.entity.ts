import { ComplejosDeportivo } from 'src/modules/complejos-deportivos/entities/complejos-deportivo.entity';
import { Comunidades } from 'src/modules/comunidades/entities/comunidade.entity';
import { Persona } from 'src/modules/personas/entities/persona.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Sector {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreSector: string;

    @ManyToOne(() => Comunidades, (comunidad) => comunidad.sector, {
        eager: true,
    })
    comunidad: Comunidades;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => Persona, (persona) => persona.sector)
    persona: Persona;

    @OneToMany(() => ComplejosDeportivo, (complejoD) => complejoD.sector)
    complejoD: ComplejosDeportivo;
}
