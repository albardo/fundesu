import { Materiale } from '../../materiales/entities/materiale.entity';
import { SuperficieInstalacion } from '../../superficie-instalacion/entities/superficie-instalacion.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

@Entity()
export class MaterialSuperficie {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadMaterial: number;

    @ManyToOne(() => Materiale, (material) => material.superficieMaterial, { eager: true })
    material: Materiale;

    @ManyToOne(() => SuperficieInstalacion, (superficie) => superficie.superficieMaterial, { eager: true })
    superficie: SuperficieInstalacion;
}
