import { PartialType } from '@nestjs/mapped-types';
import { CreateMaterialComplementoDto } from './create-material-complemento.dto';

export class UpdateMaterialComplementoDto extends PartialType(CreateMaterialComplementoDto) {}
