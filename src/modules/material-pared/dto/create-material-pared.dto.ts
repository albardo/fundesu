import { IsInt, IsUUID } from 'class-validator';
import { Materiale } from 'src/modules/materiales/entities/materiale.entity';
import { ParedInstalacion } from 'src/modules/pared-instalacion/entities/pared-instalacion.entity';

export class CreateMaterialParedDto {
    _id?: string;

    @IsInt()
    cantidadMaterial: number;

    @IsUUID()
    material: Materiale;

    @IsUUID()
    pared: ParedInstalacion;
}
