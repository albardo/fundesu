import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AtletasClubService } from './atletas-club.service';
import { CreateAtletasClubDto } from './dto/create-atletas-club.dto';
import { UpdateAtletasClubDto } from './dto/update-atletas-club.dto';

@Controller('atletas-club')
@ApiTags('Atletas-club')
export class AtletasClubController {
    constructor(private readonly atletasClubService: AtletasClubService) {}

    @Post()
    @ApiBody({ type: [CreateAtletasClubDto] })
    create(@Res() Res, @Body() createAtletasClubDto: CreateAtletasClubDto) {
        this.atletasClubService
            .create(createAtletasClubDto)
            .then((atletaClub) => {
                return Res.status(HttpStatus.CREATED).json(atletaClub);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.atletasClubService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.atletasClubService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateAtletasClubDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateAtletasClubDto: UpdateAtletasClubDto) {
        return this.atletasClubService
            .update(id, updateAtletasClubDto)
            .then((atletaClub) => {
                return Res.status(HttpStatus.ACCEPTED).json(atletaClub);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.atletasClubService.remove(id);
    }
}
