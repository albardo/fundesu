import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InstalacionesDeportiva } from '../instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Atleta } from '../atletas/entities/atleta.entity';

@Module({
    imports: [TypeOrmModule.forFeature([InstalacionesDeportiva, Atleta])],
    controllers: [DashboardController],
    providers: [DashboardService],
})
export class DashboardModule {}
