import { PartialType } from '@nestjs/mapped-types';
import { CreateImgInstalacioneDto } from './create-img-instalacione.dto';

export class UpdateImgInstalacioneDto extends PartialType(CreateImgInstalacioneDto) {}
