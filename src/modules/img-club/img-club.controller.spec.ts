import { Test, TestingModule } from '@nestjs/testing';
import { ImgClubController } from './img-club.controller';
import { ImgClubService } from './img-club.service';

describe('ImgClubController', () => {
    let controller: ImgClubController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgClubController],
            providers: [ImgClubService],
        }).compile();

        controller = module.get<ImgClubController>(ImgClubController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
