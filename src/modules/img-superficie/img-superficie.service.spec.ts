import { Test, TestingModule } from '@nestjs/testing';
import { ImgSuperficieService } from './img-superficie.service';

describe('ImgSuperficieService', () => {
    let service: ImgSuperficieService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgSuperficieService],
        }).compile();

        service = module.get<ImgSuperficieService>(ImgSuperficieService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
