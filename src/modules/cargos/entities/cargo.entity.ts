import { MiembrosClube } from 'src/modules/miembros-clubes/entities/miembros-clube.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Cargo {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreCargo: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => MiembrosClube, (miembroClub) => miembroClub.cargo)
    miembroClub: MiembrosClube;
}
