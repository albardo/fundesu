import { Test, TestingModule } from '@nestjs/testing';
import { DiscapacidadesAtletaService } from './discapacidades-atleta.service';

describe('DiscapacidadesAtletaService', () => {
    let service: DiscapacidadesAtletaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DiscapacidadesAtletaService],
        }).compile();

        service = module.get<DiscapacidadesAtletaService>(DiscapacidadesAtletaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
