import { Module } from '@nestjs/common';
import { ParroquiasService } from './parroquias.service';
import { ParroquiasController } from './parroquias.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Parroquia } from './entities/parroquia.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Parroquia])],
    controllers: [ParroquiasController],
    providers: [ParroquiasService],
})
export class ParroquiasModule {}
