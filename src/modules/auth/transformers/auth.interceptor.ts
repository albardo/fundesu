import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserTransformer } from 'src/modules/usuarios/transformers/user.transformer';
import { AuthTransformer } from './auth.transformer';

export interface Response<T> {
    statusCode: number;
    data: T;
}

@Injectable()
export class LoginInterceptor<T> implements NestInterceptor<T, Response<any>> {
    intercept(context: ExecutionContext, next: CallHandler): Observable<Response<any>> {
        const res = context.switchToHttp().getResponse();
        return next.handle().pipe(
            map((data) => ({
                statusCode: res.statusCode,
                data: AuthTransformer.transform(data),
            })),
        );
    }
}

@Injectable()
export class AuthInterceptor<T> implements NestInterceptor<T, Response<any>> {
    intercept(context: ExecutionContext, next: CallHandler): Observable<Response<any>> {
        const res = context.switchToHttp().getResponse();
        return next.handle().pipe(
            map((data) => ({
                statusCode: res.statusCode,
                data: UserTransformer.transform(data),
            })),
        );
    }
}
