import { Module } from '@nestjs/common';
import { MaterialEquipamientoEspService } from './material-equipamiento-esp.service';
import { MaterialEquipamientoEspController } from './material-equipamiento-esp.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MaterialEquipamientoEsp } from './entities/material-equipamiento-esp.entity';
import { Materiale } from '../materiales/entities/materiale.entity';

@Module({
    imports: [TypeOrmModule.forFeature([MaterialEquipamientoEsp, Materiale])],
    controllers: [MaterialEquipamientoEspController],
    providers: [MaterialEquipamientoEspService],
})
export class MaterialEquipamientoEspModule {}
