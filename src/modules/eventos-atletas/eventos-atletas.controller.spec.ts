import { Test, TestingModule } from '@nestjs/testing';
import { EventosAtletasController } from './eventos-atletas.controller';
import { EventosAtletasService } from './eventos-atletas.service';

describe('EventosAtletasController', () => {
    let controller: EventosAtletasController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [EventosAtletasController],
            providers: [EventosAtletasService],
        }).compile();

        controller = module.get<EventosAtletasController>(EventosAtletasController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
