import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MiembrosClubesService } from './miembros-clubes.service';
import { CreateMiembrosClubeDto } from './dto/create-miembros-clube.dto';
import { UpdateMiembrosClubeDto } from './dto/update-miembros-clube.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('miembros-clubes')
@ApiTags('Miembros-clubes')
export class MiembrosClubesController {
    constructor(private readonly miembrosClubesService: MiembrosClubesService) {}

    @Post()
    @ApiBody({ type: [CreateMiembrosClubeDto] })
    create(@Body() createMiembrosClubeDto: CreateMiembrosClubeDto) {
        return this.miembrosClubesService.create(createMiembrosClubeDto);
    }

    @Get()
    findAll() {
        return this.miembrosClubesService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.miembrosClubesService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMiembrosClubeDto] })
    update(@Param('id') id: string, @Body() updateMiembrosClubeDto: UpdateMiembrosClubeDto) {
        return this.miembrosClubesService.update(id, updateMiembrosClubeDto);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.miembrosClubesService.remove(id);
    }
}
