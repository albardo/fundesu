import { IsUUID } from 'class-validator';
import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Beca } from 'src/modules/becas/entities/beca.entity';

export class CreateBecasAtletaDto {
    _id?: string;

    @IsUUID()
    beca: Beca;

    @IsUUID()
    atleta: Atleta;
}
