import { IsNumber, IsString } from 'class-validator';

export class CreateBecaDto {
    _id?: string;

    @IsString()
    nombreBeca: string;

    @IsNumber()
    monto: number;
}
