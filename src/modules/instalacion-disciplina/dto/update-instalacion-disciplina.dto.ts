import { PartialType } from '@nestjs/mapped-types';
import { CreateInstalacionDisciplinaDto } from './create-instalacion-disciplina.dto';

export class UpdateInstalacionDisciplinaDto extends PartialType(CreateInstalacionDisciplinaDto) {}
