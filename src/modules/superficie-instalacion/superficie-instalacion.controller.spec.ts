import { Test, TestingModule } from '@nestjs/testing';
import { SuperficieInstalacionController } from './superficie-instalacion.controller';
import { SuperficieInstalacionService } from './superficie-instalacion.service';

describe('SuperficieInstalacionController', () => {
    let controller: SuperficieInstalacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [SuperficieInstalacionController],
            providers: [SuperficieInstalacionService],
        }).compile();

        controller = module.get<SuperficieInstalacionController>(SuperficieInstalacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
