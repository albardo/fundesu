import { Module } from '@nestjs/common';
import { AlturaPesoAtletaService } from './altura-peso-atleta.service';
import { AlturaPesoAtletaController } from './altura-peso-atleta.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlturaPesoAtleta } from './entities/altura-peso-atleta.entity';

@Module({
    imports: [TypeOrmModule.forFeature([AlturaPesoAtleta])],
    controllers: [AlturaPesoAtletaController],
    providers: [AlturaPesoAtletaService],
})
export class AlturaPesoAtletaModule {}
