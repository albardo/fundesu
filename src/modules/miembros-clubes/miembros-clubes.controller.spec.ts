import { Test, TestingModule } from '@nestjs/testing';
import { MiembrosClubesController } from './miembros-clubes.controller';
import { MiembrosClubesService } from './miembros-clubes.service';

describe('MiembrosClubesController', () => {
    let controller: MiembrosClubesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MiembrosClubesController],
            providers: [MiembrosClubesService],
        }).compile();

        controller = module.get<MiembrosClubesController>(MiembrosClubesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
