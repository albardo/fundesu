import { Module } from '@nestjs/common';
import { ImgIluminacionService } from './img-iluminacion.service';
import { ImgIluminacionController } from './img-iluminacion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgIluminacion } from './entities/img-iluminacion.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgIluminacion])],
    controllers: [ImgIluminacionController],
    providers: [ImgIluminacionService],
})
export class ImgIluminacionModule {}
