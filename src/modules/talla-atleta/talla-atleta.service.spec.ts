import { Test, TestingModule } from '@nestjs/testing';
import { TallaAtletaService } from './talla-atleta.service';

describe('TallaAtletaService', () => {
    let service: TallaAtletaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [TallaAtletaService],
        }).compile();

        service = module.get<TallaAtletaService>(TallaAtletaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
