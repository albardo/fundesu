import { ImplementosDeportivo } from 'src/modules/implementos-deportivos/entities/implementos-deportivo.entity';
import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImplementosInstalacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadImplemento: number;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.implementos, { eager: true })
    instalaciones: InstalacionesDeportiva;

    @ManyToOne(() => ImplementosDeportivo, (implemento) => implemento.implementosIsn, { eager: true })
    implemento: ImplementosDeportivo;
}
