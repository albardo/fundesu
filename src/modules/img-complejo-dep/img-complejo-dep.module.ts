import { Module } from '@nestjs/common';
import { ImgComplejoDepService } from './img-complejo-dep.service';
import { ImgComplejoDepController } from './img-complejo-dep.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgComplejoDep } from './entities/img-complejo-dep.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgComplejoDep])],
    controllers: [ImgComplejoDepController],
    providers: [ImgComplejoDepService],
})
export class ImgComplejoDepModule {}
