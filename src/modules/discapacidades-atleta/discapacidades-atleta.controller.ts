import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { DiscapacidadesAtletaService } from './discapacidades-atleta.service';
import { CreateDiscapacidadesAtletaDto } from './dto/create-discapacidades-atleta.dto';
import { UpdateDiscapacidadesAtletaDto } from './dto/update-discapacidades-atleta.dto';

@Controller('discapacidades-atleta')
@ApiTags('Discapacidades-atleta')
export class DiscapacidadesAtletaController {
    constructor(private readonly discapacidadesAtletaService: DiscapacidadesAtletaService) {}

    @Post()
    @ApiBody({ type: [CreateDiscapacidadesAtletaDto] })
    create(@Res() Res, @Body() createDiscapacidadesAtletaDto: CreateDiscapacidadesAtletaDto) {
        this.discapacidadesAtletaService
            .create(createDiscapacidadesAtletaDto)
            .then((discapacidad) => {
                return Res.status(HttpStatus.CREATED).json(discapacidad);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.discapacidadesAtletaService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.discapacidadesAtletaService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateDiscapacidadesAtletaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateDiscapacidadesAtletaDto: UpdateDiscapacidadesAtletaDto) {
        return this.discapacidadesAtletaService
            .update(id, updateDiscapacidadesAtletaDto)
            .then((discapacidad) => {
                return Res.status(HttpStatus.ACCEPTED).json(discapacidad);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.discapacidadesAtletaService.remove(id);
    }
}
