import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import 'dotenv/config';
import { json, urlencoded } from 'express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
    ClassSerializerInterceptor,
    HttpStatus,
    UnprocessableEntityException,
    ValidationError,
    ValidationPipe,
} from '@nestjs/common';
import { useContainer } from 'class-validator';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, { cors: true });

    // Interceptors
    // app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

    // Validations
    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
            validationError: {
                target: false,
            },
            exceptionFactory: (errors: ValidationError[]) => {
                const e = errors.map((error) => {
                    const tmp = {};
                    tmp[error.property] = [];
                    const keys = Object.keys(error.constraints);
                    keys.forEach((key) => {
                        tmp[error.property].push(error.constraints[key]);
                    });
                    return tmp;
                });
                return new UnprocessableEntityException(e);
            },
        }),
    );

    // Container to use repositories in validators
    useContainer(app.select(AppModule), { fallbackOnErrors: true });

    // swagger config
    if (process.env.NODE_ENV !== 'production') {
        const swagger = new DocumentBuilder()
            .setTitle('IDES')
            .setDescription('Documentacios api de los endpoints del proyecto IDES')
            .setVersion('1.0')
            .build();

        const document = SwaggerModule.createDocument(app, swagger);
        SwaggerModule.setup('api-documents', app, document);
    }

    // json limits config
    app.use(json({ limit: '50mb' }));
    app.use(urlencoded({ extended: true, limit: '50mb' }));

    // server port config
    await app.listen(process.env.SERVER_PORT);
    console.info(`Bienvenido a IDES, Server run on port: ${process.env.SERVER_PORT}`);
}
bootstrap();
