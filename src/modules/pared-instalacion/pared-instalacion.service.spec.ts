import { Test, TestingModule } from '@nestjs/testing';
import { ParedInstalacionService } from './pared-instalacion.service';

describe('ParedInstalacionService', () => {
    let service: ParedInstalacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ParedInstalacionService],
        }).compile();

        service = module.get<ParedInstalacionService>(ParedInstalacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
