import { Test, TestingModule } from '@nestjs/testing';
import { ImgComplejoDepController } from './img-complejo-dep.controller';
import { ImgComplejoDepService } from './img-complejo-dep.service';

describe('ImgComplejoDepController', () => {
    let controller: ImgComplejoDepController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgComplejoDepController],
            providers: [ImgComplejoDepService],
        }).compile();

        controller = module.get<ImgComplejoDepController>(ImgComplejoDepController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
