import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity()
export class AlturaPesoAtleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'float' })
    peso: number;

    @Column({ type: 'float' })
    estatura: number;

    @Column({ type: 'float' })
    presionArterial: number;

    @Column({ type: 'boolean' })
    statusAlturaPeso: boolean;

    @ManyToOne(() => Atleta, (atleta) => atleta.persona)
    atleta: Atleta;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ nullable: true })
    @DeleteDateColumn()
    deleteAt?: Date;
}
