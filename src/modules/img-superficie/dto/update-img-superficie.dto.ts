import { PartialType } from '@nestjs/mapped-types';
import { CreateImgSuperficieDto } from './create-img-superficie.dto';

export class UpdateImgSuperficieDto extends PartialType(CreateImgSuperficieDto) {}
