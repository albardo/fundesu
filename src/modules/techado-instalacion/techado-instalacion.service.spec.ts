import { Test, TestingModule } from '@nestjs/testing';
import { TechadoInstalacionService } from './techado-instalacion.service';

describe('TechadoInstalacionService', () => {
    let service: TechadoInstalacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [TechadoInstalacionService],
        }).compile();

        service = module.get<TechadoInstalacionService>(TechadoInstalacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
